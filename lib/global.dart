

import 'package:intl/intl.dart';

import 'models/country.dart';

class Global{
  static String keyCountry = "country";
  static String keySelectedCountry = 'selectedCountry';
  static String keyCredentials = 'credentials';
  static String keyLogin = 'login';
  static String keyApiUrl = 'api_url';
  static String appName = 'NexiVoy Comercios';
  static String appVersion = 'v020300';
  static String platform = 'ios';
  static bool isProduction = true;
  static var timeout = const Duration(seconds: 20);



  static NumberFormat getCurrencySymbol(Country selectedCountry) {
    var f;
    if (selectedCountry.completeCode == 'es-ec') {
      f = new NumberFormat.simpleCurrency(locale: 'es_US',name: 'PEN');
    } else if (selectedCountry.completeCode == 'es-es') {
      f = new NumberFormat.simpleCurrency(locale: 'es-ES');
    } else {
      f = new NumberFormat.simpleCurrency(locale: selectedCountry.completeCode);
    }
    return f;
  }
}