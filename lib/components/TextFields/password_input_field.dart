import 'package:flutter/material.dart';

class PasswordInputField extends StatelessWidget{
  IconData icon;
  String hintText;
  String prefixText;
  String suffixText;
  TextInputType textInputType;
  Color textFieldColor, iconColor;
  bool obscureText;
  double bottomMargin;
  TextStyle textStyle, hintStyle;
  var validateFunction;
  var onSaved;
  Key key;
  FocusNode focusNode;
  Widget suffixIcon;
  TextEditingController controller;

  //passing props in the Constructor.
  PasswordInputField(
      {this.key,
        this.hintText,
        this.obscureText,
        this.textInputType,
        this.textFieldColor,
        this.icon,
        this.iconColor,
        this.bottomMargin,
        this.textStyle,
        this.validateFunction,
        this.onSaved,
        this.hintStyle,
        this.prefixText,
        this.focusNode,
        this.suffixIcon,
        this.controller,
      });

  @override
  Widget build(BuildContext context) {
    return (
        new Container(
            margin: new EdgeInsets.only(bottom: bottomMargin),
            child: new DecoratedBox(
              decoration: new BoxDecoration(
                  borderRadius: new BorderRadius.all(new Radius.circular(5.0)),
                  color: textFieldColor),
              child: new TextFormField(
                style: textStyle,
                key: key,
                obscureText: obscureText,
                keyboardType: textInputType,
                validator: validateFunction,
                onSaved: onSaved,
                maxLines: 1,
                focusNode: focusNode,
                controller: controller,
                decoration: new InputDecoration(
                  border: InputBorder.none,
                  hintText: hintText,
                  hintStyle: hintStyle,
                  prefixText: prefixText,
                  suffixIcon: suffixIcon,
                  icon: new Icon(
                    icon,
                    color: iconColor,
                  ),
                ),
              ),
            )));
  }
  /*const PasswordInputField({
    this.hintText,
    this.obscureText,
    this.textInputType,
    this.textFieldColor,
    this.icon,
    this.iconColor,
    this.bottomMargin,
    this.textStyle,
    this.validateFunction,
    this.onSaved,
    this.hintStyle,
    this.prefixText,
    this.focusNode,
    this.labelText,
    this.helperText,
    this.validator,
    this.onFieldSubmitted,
    this.key,
    this.suffixText,
  });

  final String hintText;
  final String labelText;
  final String helperText;
  final FormFieldSetter<String> onSaved;
  final FormFieldValidator<String> validator;
  final ValueChanged<String> onFieldSubmitted;
  final TextStyle textStyle, hintStyle;
  final Color textFieldColor, iconColor;

  final IconData icon;
  final String prefixText;
  final String suffixText;
  final TextInputType textInputType;
  final bool obscureText;
  final double bottomMargin;
  final validateFunction;
  final Key key;
  final FocusNode focusNode;

  @override
  PasswordInputFieldState createState() => PasswordInputFieldState(this.key,
    this.hintText,
    this.obscureText,
    this.textInputType,
    this.textFieldColor,
    this.icon,
    this.iconColor,
    this.bottomMargin,
    this.textStyle,
    this.validateFunction,
    this.prefixText,
    this.focusNode,
  );
}

class PasswordInputFieldState extends State<PasswordInputField> {
  bool _obscureText = true;
  IconData icon;
  String hintText;
  String prefixText;
  String suffixText;
  TextInputType textInputType;
  Color textFieldColor, iconColor;
  bool obscureText;
  double bottomMargin;
  TextStyle textStyle, hintStyle;
  Key key;
  FocusNode focusNode;

  PasswordInputFieldState(
      this.key,
      this.hintText,
      this.obscureText,
      this.textInputType,
      this.textFieldColor,
      this.icon,
      this.iconColor,
      this.bottomMargin,
      this.textStyle,
      this.hintStyle,
      this.prefixText,
      this.focusNode,
      );

  @override
  Widget build(BuildContext context) {
    return (
        new Container(
            margin: new EdgeInsets.only(bottom: 20.0),
            child: new DecoratedBox(
                decoration: new BoxDecoration(
                    borderRadius: new BorderRadius.all(new Radius.circular(10.0)),
                    color: textFieldColor
                ),
                child:TextFormField(
                  key: key,
                  obscureText: _obscureText,
                  onSaved: widget.onSaved,
                  validator: widget.validator,
                  onFieldSubmitted: widget.onFieldSubmitted,
                  decoration: InputDecoration(
                    icon: new Icon(
                      icon,
                      color: iconColor,
                    ),
                    border: InputBorder.none,
                    filled: true,
                    hintText: hintText,
                    labelText: widget.labelText,
                    helperText: widget.helperText,
                    suffixIcon: GestureDetector(
                      onTap: () {
                        setState(() {
                          _obscureText = !_obscureText;
                        });
                      },
                      child: Icon(_obscureText ? Icons.visibility : Icons.visibility_off),
                    ),
                  ),
                )
            )
        )
    );
  }*/

}