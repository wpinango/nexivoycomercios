import 'dart:convert';
import 'package:convert/convert.dart';
import 'package:crypto/crypto.dart' as crypto;
import 'package:shared_preferences/shared_preferences.dart';

import '../models/user.dart';

class Util {
  static generateMd5(String data) {
    var content = new Utf8Encoder().convert(data);
    var md5 = crypto.md5;
    var digest = md5.convert(content);
    return hex.encode(digest.bytes);
  }

  static Future<bool> isLogin() async {
    bool isLogin = false;
    final prefs = await SharedPreferences.getInstance();
    isLogin = prefs.getBool('login');
    return isLogin;
  }

  static Future<User> getUserData() async  {
    User user;
    final prefs = await SharedPreferences.getInstance();
    user = User.fromJson(prefs.get('user'));
    return user;
  }
}