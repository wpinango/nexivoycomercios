import 'dart:convert';

import 'package:http/http.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class HttpRequest {
  static var timeout = const Duration(seconds: 50);

  static Future<Response> login(String username, String pass) async {
    /*Map<String, String> headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      //Api.keyFBToken : '1'
    };

    Map<String, String> body = {
      'username' : username.trim(),
      'password' : pass,
    };
    Response res =
    await post(Api.urlLogin, headers: headers, body: body).timeout(Api.timeout);*/
    //return res;
  }

  static Future<Response> postRequestWithAuth(String url,
      Map<String, String> headers, dynamic body, String token) async {
    try {
      Map<String, String> map = {
        'datos': json.encode(body),
      };
      print(map);
      Map<String, String> headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
      };
      final prefs = await SharedPreferences.getInstance();
      String apiVersion = prefs.get('apiVersion');
      final response = await http
          .post(url.replaceAll('?', apiVersion), headers: headers, body: map)
          .timeout(timeout);
      print(response.body);
      return response;
    } catch (e) {
      return null;
    }
    /*final store = new FlutterSecureStorage();
    String token = await store.read(key: 'token');
    Map<String, String> h = {
      'Content-Type': 'application/json',
      Api.keyAuth: token,
    };
    headers.addAll(h);
    Response res =
    await post(url, headers: headers, body: body).timeout(Api.timeout);

    if (res.statusCode == 200) {
      return res;
    } else {
      print((res.body));
      return null;
    }*/
  }

  static Future<Response> getRequestWithAuth(
      String url, Map<String, String> headers, String token) async {
    try {
      Map<String, String> headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
      };
      final prefs = await SharedPreferences.getInstance();
      String apiVersion = prefs.get('apiVersion');
      final response = await http
          .get(url.replaceAll('?', apiVersion), headers: headers)
          .timeout(timeout);
      print(response.body);
      return response;
    } catch (e) {
      return null;
    }
    /*Map<String, String> h = {
      'Content-Type': 'application/json',
    };
    headers.addAll(h);
    Response res = await get(url, headers: headers).timeout(Api.timeout);
    print(res.statusCode);
    if (res.statusCode == 200) {
      return res;
    } else {
      return null;
    }*/
  }

  static Future<Response> getRequest(
      dynamic uri, Map<String, String> headers) async {
    /*try {
      final store = new FlutterSecureStorage();
      String token = await store.read(key: 'token');
      Map<String, String> h = {
        Api.keyAuth: token,
      };
      headers.addAll(h);
      Response res = await get(uri, headers: headers).timeout(Api.timeout);
      print(res.statusCode);
      return res;
    } catch(e) {
      return null;
    }*/
  }
}
