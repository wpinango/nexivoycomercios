abstract class ResponseCallback {
  void onResponse(var response);
}