class Order {
  String orderId;
  String typeId;
  String orderManagementId;
  String reference;
  String date;
  String total;
  String preparationTime;
  String observations;
  var products;

  Order(
      {this.observations,
      this.preparationTime,
      this.total,
      this.reference,
      this.date,
      this.orderManagementId,
      this.typeId,
      this.orderId,
      this.products});

  toJson() {
    return {
      'id_pedido': orderId,
      'id_tipo': typeId,
      'id_estado_gestion': orderManagementId,
      'referencia': reference,
      'fecha': date,
      'total': total,
      'tiempo_preparacion': preparationTime,
      'observaciones': observations,
      'productos': products
    };
  }

  factory Order.fromJson(Map<String, dynamic> json) {
    return Order(
      orderId: json['id_pedido'],
      typeId: json['id_tipo'],
      orderManagementId: json['id_estado_gestion'],
      reference: json['referencia'],
      date: json['fecha'],
      total: json['total'],
      preparationTime: json['tiempo_preparacion'],
      observations: json['observaciones'],
      products: json['productos'],
    );
  }
}
