class Delivery {
  String reference;
  String ticket;
  String observations;
  String businessId;
  String business;

  Delivery({this.businessId, this.business, this.reference, this.observations, this.ticket});

  Map<String, dynamic> toJson() => {
    'referencia': reference,
    'ticket': ticket,
    'observaciones':observations,
    'id_negocio': businessId,
    'negocio':business,
  };

  factory Delivery.fromJson(Map<String, dynamic> json) {
    return Delivery(
        reference: json['reference'],
        ticket: json['ticket'],
        observations: json['observations'],
        businessId: json['business_id'],
        business: json['business']
    );
  }


}