class User {
  String name;
  String lastName;
  String email;
  String phone;
  String mobileNumber;
  String address;
  String avatar;
  String info;

  User(
      {
      this.name,
      this.lastName,
      this.address,
      this.email,
      this.mobileNumber,
      this.info,
      this.avatar,
      this.phone});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      name: json['nombre'],
      lastName: json['apellidos'],
      email: json['email'],
      phone: json['telefono'],
      mobileNumber: json['movil'],
      address: json['address'],
      info: json['info'],
      avatar: json['avatar']
    );
  }

  Map<String, dynamic> toJson() => {
    'nombre': name,
    'apellidos': lastName,
    'email':email,
    'telefono': phone,
    'movil':mobileNumber,
    'address':address,
    'avatar':avatar,
    'info':info,
  };
}
