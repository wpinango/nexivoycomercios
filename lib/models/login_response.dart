import 'package:nexivoycomercios/models/user.dart';

import 'masters.dart';

class LoginResponse {
  var user;
  int code;
  int refresh;
  String token;
  List business;
  var masters;

  LoginResponse(
      {this.user, this.refresh, this.token, this.code, this.business, this.masters});

  factory LoginResponse.fromJson(Map<String, dynamic> json) {
    return LoginResponse(
        user: json['perfil'],
        code: json['code'],
        refresh: json['refresh'],
        token: json['token'],
        business: json['negocios'],
        masters: json['maestros']
    );
  }

  Map<String, dynamic> toJson() => {
    'perfil': user,
    'code': code,
    'refresh':refresh,
    'token': token,
    'negocios':business,
    'maestros': masters,
  };
}