class Business {
  String businessId;
  String name;
  String phone;
  String phoneMobile;
  String social;
  String nif;
  String addressId;
  String address;
  String info;

  Business({this.phone, this.info, this.name, this.addressId, this.address,
    this.businessId, this.nif, this.phoneMobile, this.social});

  factory Business.fromJson(Map<String, dynamic> json) {
    return Business(
      businessId: json['id_negocio'],
      name: json['nombre'],
      phone: json['telefono'],
      phoneMobile: json['movil'],
      social: json['razon_social'],
      nif: json['nif'],
      addressId: json['id_direccion'],
      address: json['address'],
      info: json['info'],
    );
  }

  Map<String, dynamic> toJson() => {
    'id_negocio': businessId,
    'nombre': name,
    'telefono': phone,
    'movil': phoneMobile,
    'razon_social': social,
    'nif' : nif,
    'id_direccion': addressId,
    'address': address,
    'info': info,
  };
}