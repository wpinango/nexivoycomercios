import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:nexivoycomercios/components/TextFields/input_field.dart';
import 'package:nexivoycomercios/components/buttons/rounded_button.dart';
import 'package:nexivoycomercios/components/buttons/text_button.dart';

import 'package:nexivoycomercios/models/country.dart';
import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';

import '../global.dart';
import '../theme.dart';

class OrderDialog extends StatefulWidget {
  final order;
  final status;
  final Country selectedCountry;

  const OrderDialog({Key key, this.status, this.selectedCountry, this.order})
      : super(key: key);

  @override
  OrderDialogState createState() =>
      new OrderDialogState(order, status, selectedCountry);
}

class OrderDialogState extends State<OrderDialog> {
  var order;
  var status;
  Country selectedCountry;
  TextEditingController commentController = new TextEditingController();
  TextEditingController timeController = new TextEditingController();

  OrderDialogState(this.order, this.status, this.selectedCountry) {
    commentController.text = order['observaciones'];
    timeController.text = order['tiempo_preparacion'];

  }

  Color color = Colors.white;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: new IconThemeData(color: Colors.white),
        title: Container(
            child: Text(
              status == 4 ? 'Confirmar Pedido' : 'Rechazar Pedido',
              style: GoogleFonts.varelaRound(color: white),
            )),
      ),
      body: SingleChildScrollView(
        child: Container(
            alignment: Alignment.center,
            margin: EdgeInsets.only(top: 64),
            child: getBusinessOrders(order, MediaQuery.of(context).size)),
      ),
      resizeToAvoidBottomPadding: true,
    );
  }

  Widget getBusinessOrders(var order, Size size) {
    var format = Global.getCurrencySymbol(selectedCountry);
    List<Widget> widgets = new List();
    widgets.add(Container(
      padding: EdgeInsets.all(4),
      margin: EdgeInsets.all(4),
      height: size.height / 20,
      decoration: BoxDecoration(
        color: textFieldColor,
        borderRadius: BorderRadius.circular(5.0),
        border: Border.all(color: white, style: BorderStyle.solid, width: 0.80),
      ),
      width: size.width / 1.1,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            order['fecha'].toString().split(" ")[0],
            style: GoogleFonts.varelaRound(),
          ),
          Text(order['fecha'].toString().split(" ")[1],
              style: GoogleFonts.varelaRound()),
          Text('Ref.' + order['referencia'],
              style: GoogleFonts.varelaRound(fontWeight: FontWeight.bold)),
        ],
      ),
    ));
    widgets.add(Container(
      margin: EdgeInsets.all(8),
      width: size.width / 1.4,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Importe ' + format.format(double.parse(order['total'])),
            style: GoogleFonts.varelaRound(fontWeight: FontWeight.bold),
          ),
          Text('Tiempo de preparacion ' + order['tiempo_preparacion'],
              style: GoogleFonts.varelaRound(fontWeight: FontWeight.bold)),
          order['observaciones'] != null
              ? Text(
            'Observaciones ' + order['observaciones'],
            style: GoogleFonts.varelaRound(fontWeight: FontWeight.bold),
          )
              : null,
        ],
      ),
    ));
    widgets.add(Container(
      width: size.width / 1.2,
      child: Divider(),
    ));
    for (var product in order['productos']) {
      widgets.add(Container(
        width: size.width / 1.5,
        child: Text(
            '-' +
                product['cantidad'] +
                'x ' +
                product['categoria'] +
                '->' +
                product['producto'],
            style: GoogleFonts.varelaRound()),
      ));
    }
    widgets.add(Padding(
      padding: EdgeInsets.all(8),
    ));
    if (status == 4) {

      widgets.add(
        GestureDetector(
          child: AbsorbPointer(
            child: Container(
              width: size.width / 1.2,
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(
                  color: Color(0xffe2e2e2),
                ),
                borderRadius: BorderRadius.circular(5),
              ),
              child: new InputField(
                  obscureText: false,
                  hintText: 'Tiempo de espera',
                  controller: timeController,
                  textInputType: TextInputType.emailAddress,
                  textStyle: GoogleFonts.varelaRound(textStyle: textStyle),
                  hintStyle: hintStyle,
                  textCapitalization: TextCapitalization.none,
                  //textFieldColor: textFieldColor,
                  bottomMargin: 1,
                  //validateFunction: validations.validateTextInput,
                  onSaved: (String value) {
                    order['cambio_horario'] = value;
                  }),
            ),
          ),
          onTap: () {
            DateTime t;
            showDialog(
                context: context,
                child: AlertDialog(
                  title: Text('Confirma el tiempo'),
                  actions: [
                    TextButton(
                      buttonName: 'Aceptar',
                      onPressed: () {
                        timeController.text = new DateFormat('HH:mm:ss')
                            .format(t); //_dateTime = time;
                        Navigator.of(context).pop();
                        setState(() {
                          order['cambio_horario'] = timeController.text;

                        });
                      },
                    )
                  ],
                  content: TimePickerSpinner(
                    isForce2Digits: true,
                    minutesInterval: 15,
                    time: new DateFormat("HH:mm:ss").parse(timeController.text),
                    highlightedTextStyle:
                    GoogleFonts.varelaRound(textStyle: TextStyle()),
                    normalTextStyle:
                    GoogleFonts.varelaRound(textStyle: TextStyle()),
                    is24HourMode: true,
                    onTimeChange: (time) {
                      setState(() {
                        t = time;
                      });
                    },
                  ),
                ));
          },
        ),
      );
    } else if (status == 2) {
      widgets.add(Container(
        width: size.width / 1.2,
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(
            color: Color(0xffe2e2e2),
          ),
          borderRadius: BorderRadius.circular(5),
        ),
        child: new InputField(
          controller: commentController,
          obscureText: false,
          hintText: 'Motivo del rechazo',
          textInputType: TextInputType.emailAddress,
          textStyle: GoogleFonts.varelaRound(textStyle: textStyle),
          hintStyle: hintStyle,
          textCapitalization: TextCapitalization.none,
          bottomMargin: 1,
          onSaved: (String text) {
            commentController.text = (text);
            order['motivos_rechazo'] = text;
          },
        ),
      ));
    }
    widgets.add(Padding(
      padding: EdgeInsets.all(8),
    ));
    widgets.add(
      RoundedButton(
        buttonName: status == 4 ? "Confirmar Pedido" : "Rechazar Pedido",
        onTap: () {
          if (status == 2) {
            order['observaciones'] = commentController.text;
          }
          Navigator.pop(context, order);
        },
        width: size.width / 1.2,
        height: size.height / 13,
        bottomMargin: 10.0,
        borderWidth: 0.0,
        buttonColor: blueNexi,
      ),
    );
    widgets.add(
      RoundedButton(
        buttonName: 'Atras',
        onTap: () => Navigator.of(context).pop(),
        width: size.width / 1.2,
        height: size.height / 13,
        bottomMargin: 10.0,
        borderWidth: 0.0,
        buttonColor: blueNexi,
      ),
    );
    return Column(
      children: widgets,
    );
  }
}
