import 'package:nexivoycomercios/database/order_db_configuration.dart';
import 'package:nexivoycomercios/models/order.dart';
import 'package:sembast/sembast.dart';

class OrderDao {
  static const String folderName = "order";
  final orderFolder = intMapStoreFactory.store(folderName);

  Future<Database> get _db async => await AppDatabase.instance.database;

  Future insertOrder(Order order) async {
    await orderFolder.add(await _db, order.toJson());
    print('UserOrder Inserted successfully !!');
  }

  Future updateOrder(Order order) async {
    final finder = Finder(filter: Filter.byKey(order.reference));
    await orderFolder.update(await _db, order.toJson(), finder: finder);
  }

  Future delete(Order order) async {
    final finder = Finder(filter: Filter.byKey(order.reference));
    await orderFolder.delete(await _db);

  }

  Future deleteAll() async {
    List<Order> order = await getAllOrders();
    for (var o in order) {
      delete(o);
    }
  }

  Future<List<Order>> getAllOrders() async {
    final recordSnapshot = await orderFolder.find(await _db);
    return recordSnapshot.map((snapshot) {
      final student = Order.fromJson(snapshot.value);
      return student;
    }).toList();
  }

  Future insertOrders(List orders) async {
    List myOrders = orders;
    try {
      myOrders.first['pedidos'][0]['referencia'] = '1';
      orders.removeWhere((item) => !orders.contains(myOrders));
      print(orders);
    }catch(e) {
      e.toString();
    }
  }
}
