import 'dart:convert';

import 'package:nexivoycomercios/components/TextFields/input_field.dart';
import 'package:nexivoycomercios/components/TextFields/password_input_field.dart';
import 'package:nexivoycomercios/components/buttons/rounded_button.dart';
import 'package:nexivoycomercios/components/buttons/text_button.dart';
import 'package:nexivoycomercios/models/country.dart';
import 'package:nexivoycomercios/models/credentials.dart';
import 'package:nexivoycomercios/models/login_response.dart';
import 'package:nexivoycomercios/services/validations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../../api_url.dart';
import '../../global.dart';
import '../../routes.dart';
import '../../theme.dart';
import '../../utils/util.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key key}) : super(key: key);

  @override
  LoginScreenState createState() => new LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  BuildContext context;
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController scrollController = new ScrollController();
  bool autoValidate = false;
  Validations validations = new Validations();
  bool isInAsyncCall = false;
  var timeout = const Duration(seconds: 10);
  bool isHidePass = true;
  Credentials credentials = new Credentials();
  List<Country> countryList = new List();
  List<String> values = ['España'];
  String country;
  bool isLogin = false;
  Country selectedCountry;

  @override
  initState() {
    super.initState();
    initVars();
  }

  initVars() async {
    checkUserLogin(0);
  }

  setCountry() async {
    Country c =  new Country();
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('selectedCountry', json.encode(selectedCountry));
    prefs.setString(Global.keyApiUrl,
        selectedCountry.protocol + '://' + selectedCountry.url);
  }

  onPressed(String routeName) {
    Navigator.of(context).pushNamed(routeName);
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  void checkUserLogin(int state) async {
    final prefs = await SharedPreferences.getInstance();
    selectedCountry = new Country();
    selectedCountry.name = 'Mexico';
    selectedCountry.url = ApiUrl.urlPrefix;
    selectedCountry.protocol = "http";
    selectedCountry.code = "mx";
    selectedCountry.completeCode = "es-mx";
    prefs.setString('selectedCountry', json.encode(selectedCountry));
    if (prefs.getBool('login') != null) {
      isLogin = prefs.getBool('login');
      if (isLogin) {
        if (state != 1) {
          //refreshData();
          getSelectedCountry();
          credentials = Credentials.fromJson(
              json.decode(prefs.get(Global.keyCredentials)));
          doLogin();
        }
      } else {
        requestVersion();
      }
    } else {
      requestVersion();
    }
  }

  void getSelectedCountry() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.get('selectedCountry') != null) {
      setState(() {
        selectedCountry =
            Country.fromJson(json.decode(prefs.get('selectedCountry')));
      });
    }
    if (selectedCountry.name == null) {
      requestVersion();
    }
  }

  void requestVersion() async {
    showProgressDialog();
    try {
      var body = {'version': 'v020100', 'platform': 'android', 'token': 0};
      Map<String, String> map = {
        'datos': json.encode(body),
      };
      print(map);
      Map<String, String> headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
      };
      final response = await http
          .post('http://' +ApiUrl.urlPrefix + ApiUrl.urlGetVersion,
              headers: headers, body: map)
          .timeout(timeout);
      print(response.body);
      if (response.body != "") {
        handleVersionResponse(response.body);
      } else {
        handleVersionResponse("");
      }
    } catch (e) {
      handleVersionResponse("");
      print('algo salio mal ' + e.toString());
    }
  }

  void handleVersionResponse(final responseBody) async {
    cancelProgressDialog();
    if (responseBody != "") {
      Map<String, dynamic> map = json.decode(responseBody);
      if (map['resultado']['code'] == 0) {
        String apiVersion = map['resultado']['vapi'];
        final prefs = await SharedPreferences.getInstance();
        prefs.setString('apiVersion', apiVersion);
        for (var item in map['resultado']['businesses']) {
          Country country = new Country();
          country.name = item[0].toString().replaceAll('Aloha', 'España');
          country.url = item[1];
          country.code = item[3].toString().split('-')[1];
          country.completeCode = item[3];
          country.protocol = item[6];
          countryList.add(country);
          if (!values.contains(country.name)) {
            values.add(country.name);
          }
        }
      }
    } else {
      cancelProgressDialog();
      showInSnackBar('Algo salio mal');
    }
  }

  onSelectedCountry() async {
    if (country != null) {
      for (var c in countryList) {
        if (c.name == country) {
          setState(() {
            selectedCountry = c;
          });
          final prefs = await SharedPreferences.getInstance();
          prefs.setString('selectedCountry', json.encode(selectedCountry));
          prefs.setString(Global.keyApiUrl,
              selectedCountry.protocol + '://' + selectedCountry.url);
        }
      }
    } else {
      showInSnackBar("Debe seleccionar un pais");
    }
  }

  void doLogin() async {
    showProgressDialog();
    var body = {
      'username': credentials.user,
      'password': Util.generateMd5(credentials.password),
      'avatar': 0,
      'ts': '0'
    };
    Map<String, String> map = {
      'datos': json.encode(body),
    };
    print(map);
    Map<String, String> headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    final prefs = await SharedPreferences.getInstance();
    String apiVersion = prefs.get('apiVersion');
    //Country country = Country.fromJson(prefs.get('country'));
    try {
      final response = await http
          .post(
              selectedCountry.protocol +
                  '://' +
                  selectedCountry.url +
                  ApiUrl.urlLogin.replaceAll('?', apiVersion),
              headers: headers,
              body: map)
          .timeout(timeout);
      print(response.body);
      print('url : ' + selectedCountry.url);
      if (response.body != "") {
        handleLoginResponse(response.body);
      } else {
        handleLoginResponse("");
      }
    } catch (e) {
      handleLoginResponse("");
    }
  }

  void handleLoginResponse(final response) async {
    cancelProgressDialog();
    if (response != "") {
      Map<String, dynamic> map = json.decode(response);
      if (map['resultado']['code'] == 0) {
        print(json.encode(map['resultado']));
        try {
          LoginResponse loginResponse = LoginResponse.fromJson(
              json.decode(json.encode(map['resultado'])));

          final prefs = await SharedPreferences.getInstance();
          //print(json.encode(loginResponse));
          setState(() {
            prefs.setString(Global.keyCredentials, json.encode(credentials));
            prefs.setString('loginData', json.encode(loginResponse));
            prefs.setBool('login', true);
          });
          Routes.replacementScreen(context, '/Orders');
        } catch (e) {
          print(e.toString());
        }
      } else if (map['resultado']['code'] == -1) {
        showInSnackBar('El usuario o contraseña son incorrectos');
      } else if (map['resultado']['code'] == -2) {
        showInSnackBar('Servicio no disponible');
      }
    } else if (response == "httpExcep") {
      showInSnackBar("Revice la conexion a internet");
    } else {
      showInSnackBar("Algo salio mal");
    }
  }

  void handleFormSubmitted() {
    final FormState form = formKey.currentState;
    if (!form.validate()) {
      autoValidate = true; // Start validating on every change.
      showInSnackBar('Por favor corrija los errores antes de continuar.');
    } else {
      form.save();
      print("buttonClicked");
      //onSelectedCountry();
      doLogin();
    }
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      key: scaffoldKey,
      body: ModalProgressHUD(
        child: SingleChildScrollView(
          child: new Container(
            width: screenSize.width,
            height: screenSize.height,
            padding: new EdgeInsets.all(16.0),
            decoration: new BoxDecoration(image: DecorationImage(image: AssetImage('assets/fondo.png'),fit: BoxFit.fill)),
            child: new Column(
              children: <Widget>[
                new Container(
                  height: screenSize.height / 1.2,
                  width: screenSize.width / 1.4,
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.all(20),
                      ),
                      new Image.asset('assets/NEXI-VOY_logotipo_WEB_BLANCO.png'),
                      new Form(
                        key: formKey,
                        autovalidate: autoValidate,
                        child: new Column(
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(20),
                            ),
                            new InputField(
                                hintText: "Correo",
                                obscureText: false,
                                textInputType: TextInputType.emailAddress,
                                textStyle: textStyle,
                                hintStyle: hintStyle,
                                textCapitalization: TextCapitalization.none,
                                textFieldColor: textFieldColor,
                                icon: Icons.mail_outline,
                                iconColor: const Color(0xFF386080),
                                bottomMargin: 8.0,
                                validateFunction: validations.validateEmail,
                                onSaved: (String email) {
                                  credentials.user = email;
                                }),
                            Padding(padding: EdgeInsets.all(8),),
                            new PasswordInputField(
                                hintText: "Password",
                                obscureText: isHidePass,
                                textInputType: TextInputType.text,
                                textStyle: textStyle,
                                hintStyle: hintStyle,
                                textFieldColor: textFieldColor,
                                icon: Icons.lock_open,
                                iconColor: const Color(0xFF386080),
                                bottomMargin: 8.0,
                                suffixIcon: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      isHidePass = !isHidePass;
                                    });
                                  },
                                  child: Icon(!isHidePass
                                      ? Icons.visibility
                                      : Icons.visibility_off),
                                ),
                                validateFunction: validations.validatePassword,
                                onSaved: (String password) {
                                  credentials.password = password;
                                }),
                            new TextButton(
                                buttonName: "Olvide mi Contraseña",
                                onPressed: () => onPressed("/PasswordRecovery"),
                                buttonTextStyle: buttonTextStyle),
                            new RoundedButton(
                              buttonName: "Iniciar Sesion",
                              onTap: handleFormSubmitted,
                              width: screenSize.width,
                              height: screenSize.height / 13,
                              bottomMargin: 10.0,
                              borderWidth: 0.0,
                              buttonColor: blueNexi,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
      resizeToAvoidBottomPadding: true,
    );
  }
}
