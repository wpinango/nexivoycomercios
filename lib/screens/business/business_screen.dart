import 'dart:convert';

import 'package:google_fonts/google_fonts.dart';
import 'package:nexivoycomercios/models/business.dart';
import 'package:nexivoycomercios/models/login_response.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../theme.dart';

class BusinessScreen extends StatefulWidget {
  const BusinessScreen({Key key}) : super(key: key);

  @override
  BusinessScreenState createState() => new BusinessScreenState();
}

class BusinessScreenState extends State<BusinessScreen> {
  BuildContext context;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isInAsyncCall = false;
  Business business;
  List<Business> businesses = new List();
  LoginResponse loginResponse;

  @override
  void initState() {
    super.initState();
    getBusiness();
  }

  void getBusiness() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.get('selectedCountry') != null) {
      setState(() {
        loginResponse =
            LoginResponse.fromJson(json.decode(prefs.get('loginData')));
        for (var a in loginResponse.business) {
          Business business = new Business();
          business.name = a['nombre'];
          business.info = a['info'];
          business.address = a['address'];
          business.addressId = a['id_direccion'];
          business.social = a['razon_social'];
          business.nif = a['nif'];
          business.phoneMobile = a['movil'];
          business.phone = a['telefono'];
          business.businessId = a['id_negocio'];
          businesses.add(business);
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    var height = AppBar().preferredSize.height;
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        iconTheme: new IconThemeData(color: Colors.white),
        title: Container(
          height: height / 1.8,
          child: Image(
            image: AssetImage('assets/NEXI-VOY_logotipo_WEB_BLANCO.png'),
          ),
        ),
        centerTitle: true,
        actions: <Widget>[],
        backgroundColor: primaryColor,
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: ModalProgressHUD(
        child: new Container(
          height: screenSize.height,
          width: screenSize.width,
          padding: new EdgeInsets.all(8.0),
          child: new Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(8),
                child: Text(
                  'Negocios',
                  style: GoogleFonts.varelaRound(
                      textStyle: TextStyle(
                          //color: primaryColor,
                          //fontWeight: FontWeight.bold,
                          fontSize: 18)),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8),
              ),
              Container(
                height: screenSize.height / 1.5,
                width: screenSize.width,
                child: new ListView.builder(
                  itemCount: businesses.length,
                  itemBuilder: (BuildContext context, int index) {
                    return new Container(
                      padding: EdgeInsets.all(8),
                      child: Column(
                        //crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            businesses[index].name,
                            style: GoogleFonts.varelaRound(
                                textStyle:
                                    TextStyle(fontWeight: FontWeight.bold)),
                          ),
                          Padding(
                            padding: EdgeInsets.all(16),
                          ),
                          businesses[index].social != null
                              ? Container(
                            margin: EdgeInsets.all(4),
                                child: Row(
                                    children: <Widget>[
                                      Text(
                                        'Razon social: ',
                                        style: GoogleFonts.varelaRound(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.bold)),
                                      ),
                                      Container(
                                        child: Text(
                                          businesses[index].social,
                                          style: GoogleFonts.varelaRound(
                                              textStyle: TextStyle()),
                                        ),
                                        width: screenSize.width / 2.3,
                                      ),
                                    ].where((child) => child != null).toList(),
                                  ),
                              )
                              : null,
                          businesses[index].nif != null
                              ? Container(
                            margin: EdgeInsets.all(4),
                            child: Row(
                                    children: <Widget>[
                                      Text(
                                        'Nif/Cif: ',
                                        style: GoogleFonts.varelaRound(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.bold)),
                                      ),
                                      Text(
                                        businesses[index].nif,
                                        style: GoogleFonts.varelaRound(
                                            textStyle: TextStyle()),
                                      ),
                                    ].where((child) => child != null).toList(),
                                  ),
                              )
                              : null,
                          businesses[index].address != null
                              ? Container(
                            margin: EdgeInsets.all(4),
                                child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        'Direccion: ',
                                        style: GoogleFonts.varelaRound(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.bold)),
                                      ),
                                      Container(
                                        child: Text(
                                          businesses[index].address,
                                          style: GoogleFonts.varelaRound(
                                              textStyle: TextStyle()),
                                        ),
                                        width: screenSize.width / 2,
                                      ),
                                    ].where((child) => child != null).toList(),
                                  ),
                              )
                              : null,
                          businesses[index].info != null
                              ? Container(

                            margin: EdgeInsets.all(4),
                                child: Row(
                                    children: <Widget>[
                                      Text(
                                        'Info direccion: ',
                                        style: GoogleFonts.varelaRound(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.bold)),
                                      ),
                                      Container(
                                        width: screenSize.width / 2,
                                        child: Text(businesses[index].info,
                                            style: GoogleFonts.varelaRound(
                                                textStyle: TextStyle())),
                                      ),
                                    ].where((child) => child != null).toList(),
                                  ),
                              )
                              : null,
                          businesses[index].phone != null
                              ? Container(
                              margin: EdgeInsets.all(4),
                            child: Row(
                                    children: <Widget>[
                                      Text(
                                        'Telefono: ',
                                        style: GoogleFonts.varelaRound(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.bold)),
                                      ),
                                      Text(
                                        businesses[index].phone,
                                        style: GoogleFonts.varelaRound(
                                            textStyle: TextStyle()),
                                      ),
                                    ].where((child) => child != null).toList(),
                                  ),
                              )
                              : null,
                          businesses[index].phoneMobile != null
                              ? Container(
                            margin: EdgeInsets.all(4),
                            child: Row(
                                    children: <Widget>[
                                      Text(
                                        'Movil: ',
                                        style: GoogleFonts.varelaRound(
                                            textStyle: TextStyle(
                                                fontWeight: FontWeight.bold)),
                                      ),
                                      Text(businesses[index].phoneMobile,
                                          style: GoogleFonts.varelaRound(
                                              textStyle: TextStyle())),
                                    ].where((child) => child != null).toList(),
                                  ),
                              )
                              : null,
                          new Divider(color: Colors.grey)
                        ].where((child) => child != null).toList(),
                      ),
                    );
                  },
                ),
              )
            ].where((child) => child != null).toList(),
          ),
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
      resizeToAvoidBottomPadding: false,
    );
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }
}
