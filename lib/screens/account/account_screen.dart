import 'dart:convert';

import 'package:google_fonts/google_fonts.dart';
import 'package:nexivoycomercios/components/TextFields/input_field.dart';
import 'package:nexivoycomercios/components/TextFields/password_input_field.dart';
import 'package:nexivoycomercios/components/buttons/rounded_button.dart';
import 'package:nexivoycomercios/dialogs/dialog.dart';
import 'package:nexivoycomercios/models/country.dart';
import 'package:nexivoycomercios/models/credentials.dart';
import 'package:nexivoycomercios/models/login_response.dart';
import 'package:nexivoycomercios/models/user.dart';
import 'package:nexivoycomercios/services/validations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../../api_url.dart';
import '../../global.dart';
import '../../theme.dart';
import '../../utils/util.dart';

class AccountScreen extends StatefulWidget {
  const AccountScreen({Key key}) : super(key: key);

  @override
  AccountScreenState createState() => new AccountScreenState();
}

class AccountScreenState extends State<AccountScreen> {
  BuildContext context;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isInAsyncCall = false;
  Validations validations = new Validations();
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  ScrollController scrollController = new ScrollController();
  bool autoValidate = false;
  var timeout = const Duration(seconds: 30);
  List<Country> countryList = new List();
  List<String> values = ['España'];
  String country;
  Country selectedCountry;
  LoginResponse loginResponse;
  TextEditingController nameController = new TextEditingController();
  TextEditingController lastNameController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController phoneController = new TextEditingController();
  TextEditingController mobileController = new TextEditingController();
  TextEditingController addressController = new TextEditingController();
  TextEditingController infoController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController oldPassController = new TextEditingController();
  TextEditingController repeatPassController = new TextEditingController();
  User user;
  String password;

  @override
  void initState() {
    super.initState();
    getUserData();
    getSelectedCountry();
  }

  void getSelectedCountry() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.get('selectedCountry') != null) {
      setState(() {
        selectedCountry =
            Country.fromJson(json.decode(prefs.get('selectedCountry')));
        country = selectedCountry.name;
      });
    }
    if (selectedCountry.name == null) {
      requestVersion();
    }
  }

  void getUserData() async {
    user = new User();
    final prefs = await SharedPreferences.getInstance();
    if (prefs.get('selectedCountry') != null) {
      setState(() {
        loginResponse =
            LoginResponse.fromJson(json.decode(prefs.get('loginData')));
        print(loginResponse.user['nombre']);
        nameController.text = loginResponse.user['nombre'];
        lastNameController.text = loginResponse.user['apellidos'];
        emailController.text = loginResponse.user['email'];
        phoneController.text = loginResponse.user['telefono'];
        mobileController.text = loginResponse.user['movil'];
        addressController.text = loginResponse.user['address'];
        infoController.text = loginResponse.user['info'];
      });
    }
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  void requestVersion() async {
    showProgressDialog();
    try {
      var body = {'version': 'v020405', 'platform': 'android', 'token': 0};
      Map<String, String> map = {
        'datos': json.encode(body),
      };
      print(map);
      Map<String, String> headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
      };
      final response = await http
          .post('http://' +ApiUrl.urlPrefix + ApiUrl.urlGetVersion,
              headers: headers, body: map)
          .timeout(timeout);
      print(response.body);
      if (response.body != "") {
        handleVersionResponse(response.body);
      } else {
        handleVersionResponse("");
      }
    } catch (e) {
      handleVersionResponse("");
      print('algo salio mal ' + e.toString());
    }
  }

  void handleVersionResponse(final responseBody) async {
    cancelProgressDialog();
    if (responseBody != "") {
      Map<String, dynamic> map = json.decode(responseBody);
      for (var item in map['resultado']['businesses']) {
        Country country = new Country();
        country.name = item[0].toString().replaceAll('Aloha', 'España');
        country.url = item[1];
        country.code = item[3].toString().split('-')[1];
        country.completeCode = item[3];
        country.protocol = item[6];
        countryList.add(country);
        if (!values.contains(country.name)) {
          values.add(country.name);
        }
      }
    } else {
      cancelProgressDialog();
      showInSnackBar('Algo salio mal');
    }
  }

  void updateProfile() async {
    showProgressDialog();
    final prefs = await SharedPreferences.getInstance();
    String apiVersion = prefs.get('apiVersion');
    try {
      var body = {
        'email': user.email,
        'passowrd': Util.generateMd5(password),
        'nombre': user.name,
        'apellidos': user.lastName,
        'telefono': user.phone,
        'movil': user.mobileNumber,
        'address': user.address,//'Ciudad de Mexico',//user.address,
        'info': user.info
      };
      var m = {
        'profile': body,
        'token':loginResponse.token,
      };
      Map<String, String> map = {
        'datos': json.encode(m),
      };
      print(map);
      Map<String, String> headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
      };
      final response = await http
          .post(selectedCountry.protocol +
          '://' + selectedCountry.url + ApiUrl.urlUpdateProfile.replaceAll('?', apiVersion),
              headers: headers, body: map)
          .timeout(timeout);
      print(response.body);
      if (response.body != "") {
        handleUpdateUserResponse(response.body);
      } else {
        handleUpdateUserResponse("");
      }
    } catch (e) {
      handleVersionResponse("");
      print('algo salio mal ' + e.toString());
    }
  }

  void handleUpdateUserResponse(final responseBody) async {
    cancelProgressDialog();
    if (responseBody != "") {
      Map<String, dynamic> map = json.decode(responseBody);
      if (map['resultado']['code'] == 0) {
        onSelectedCountry();
        Credentials credentials = new Credentials();
        credentials.password = password;
        credentials.user = user.email;
        final prefs = await SharedPreferences.getInstance();
        setState(() {
          prefs.setString(Global.keyCredentials, json.encode(credentials));
          //prefs.setString('loginData', json.encode(loginResponse));
          //prefs.setBool('login', true);
          passwordController.clear();
          oldPassController.clear();
          repeatPassController.clear();
        });
        MyDialog.showNativePopUpWith2Buttons(context, 'Correcto', 'Se actualizo los datos correctamente', 'Cancelar', 'Aceptar');
      }
    } else {
      cancelProgressDialog();
      showInSnackBar('Algo salio mal');
    }
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    var height = AppBar().preferredSize.height;
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        iconTheme: new IconThemeData(color: Colors.white),
        title: Container(
          height: height / 1.8,
          child: Image(
            image: AssetImage('assets/NEXI-VOY_logotipo_WEB_BLANCO.png'),
          ),
        ),
        centerTitle: true,
        actions: <Widget>[],
        backgroundColor: primaryColor,
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: ModalProgressHUD(
        child: SingleChildScrollView(
          controller: scrollController,
          child: new Container(
            //height: screenSize.height * 1.5,
            width: screenSize.width,
            padding: new EdgeInsets.all(8.0),
            child: new Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(8),
                  child: Text(
                    'Mis datos',
                    style: GoogleFonts.varelaRound(textStyle: TextStyle(
                        fontSize: 22)),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                ),
                CircleAvatar(
                  radius: screenSize.height / 12,
                  child: Container(
                    width: screenSize.width / 4,
                    height: screenSize.height / 12,
                    child: Image.asset('assets/user.png'),
                  ),),
                /*Container(
                  child: Image.asset('assets/user.png'),
                ),*/
                /*Theme(
                    data: new ThemeData(
                        canvasColor: blueNexi,
                        primaryColor: blueNexi,
                        accentColor: blueNexi,
                        hintColor: blueNexi),
                    child: Container(
                        margin: new EdgeInsets.only(bottom: 12.0, top: 12.0),
                        height: screenSize.height / 13,
                        width: screenSize.width / 1.1,
                        padding: EdgeInsets.symmetric(horizontal: 10.0),
                        decoration: BoxDecoration(
                          color: blueNexi,
                          borderRadius: BorderRadius.circular(5.0),
                          border: Border.all(
                              color: white,
                              style: BorderStyle.solid,
                              width: 0.80),
                        ),
                        child: new DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                              hint: Text(
                                'Seleccione un pais',
                                style: GoogleFonts.varelaRound(textStyle: TextStyle(color: white)),
                              ),
                              items: values.map<DropdownMenuItem<String>>(
                                  (String value) {
                                return DropdownMenuItem<String>(
                                  value: country,
                                  child: Text(
                                    country,
                                    style: TextStyle(color: white),
                                  ),
                                );
                              }).toList(),
                              onChanged: (String value) {
                                setState(() {
                                  country = value;
                                });
                              },
                              value: country),
                        ))),*/
                Container(
                  padding: new EdgeInsets.all(7.0),
                  child: Form(
                    key: formKey,
                    autovalidate: autoValidate,
                    child: new Column(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              color: Color(0xffe2e2e2),
                            ),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: new InputField(
                              hintText: 'Nombre',
                              obscureText: false,
                              textInputType: TextInputType.emailAddress,
                              textStyle: GoogleFonts.varelaRound(textStyle: textStyle),
                              hintStyle: hintStyle,
                              controller: nameController,
                              textCapitalization: TextCapitalization.none,
                              bottomMargin: 1,
                              validateFunction: validations.validateTextInput,
                              onSaved: (String value) {
                                user.name = value;
                              }),
                        ),
                        Padding(padding: EdgeInsets.all(4),),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              color: Color(0xffe2e2e2),
                            ),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: new InputField(
                              hintText: 'Apellidos',
                              obscureText: false,
                              textInputType: TextInputType.emailAddress,
                              textStyle: GoogleFonts.varelaRound(textStyle: textStyle),
                              hintStyle: hintStyle,
                              controller: lastNameController,
                              textCapitalization: TextCapitalization.none,
                              bottomMargin: 1,
                              validateFunction: validations.validateTextInput,
                              onSaved: (String value) {
                                user.lastName = value;
                              }),
                        ),
                        Padding(padding: EdgeInsets.all(4),),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              color: Color(0xffe2e2e2),
                            ),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: new InputField(
                              hintText: 'Email',
                              obscureText: false,
                              textInputType: TextInputType.emailAddress,
                              textStyle: GoogleFonts.varelaRound(textStyle: textStyle),
                              hintStyle: hintStyle,
                              controller: emailController,
                              textCapitalization: TextCapitalization.none,
                              bottomMargin: 8.0,
                              validateFunction: validations.validateTextInput,
                              onSaved: (String value) {
                                user.email = value;
                              }),
                        ),
                        Padding(padding: EdgeInsets.all(4),),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              color: Color(0xffe2e2e2),
                            ),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: new InputField(
                              hintText: 'Telefono',
                              obscureText: false,
                              textInputType: TextInputType.emailAddress,
                              textStyle: GoogleFonts.varelaRound(textStyle: textStyle),
                              hintStyle: hintStyle,
                              controller: phoneController,
                              textCapitalization: TextCapitalization.none,
                              bottomMargin: 8.0,
                              //validateFunction: validations.validateTextInput,
                              onSaved: (String value) {
                                user.phone = value;
                              }),
                        ),
                        Padding(padding: EdgeInsets.all(4),),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              color: Color(0xffe2e2e2),
                            ),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: new InputField(
                              hintText: 'Telefono movil',
                              obscureText: false,
                              textInputType: TextInputType.emailAddress,
                              textStyle: GoogleFonts.varelaRound(textStyle: textStyle),
                              hintStyle: hintStyle,
                              controller: mobileController,
                              textCapitalization: TextCapitalization.none,
                              bottomMargin: 8.0,
                              //validateFunction: validations.validateTextInput,
                              onSaved: (String value) {
                                user.mobileNumber = value;
                              }),
                        ),
                        Padding(padding: EdgeInsets.all(4),),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              color: Color(0xffe2e2e2),
                            ),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: new InputField(
                              hintText: 'Direccion',
                              obscureText: false,
                              textInputType: TextInputType.emailAddress,
                              textStyle: GoogleFonts.varelaRound(textStyle: textStyle),
                              hintStyle: hintStyle,
                              controller: addressController,
                              enable: false,
                              textCapitalization: TextCapitalization.none,
                              bottomMargin: 8.0,
                              //validateFunction: validations.validateTextInput,
                              onSaved: (String value) {
                                user.address = value;
                              }),
                        ),
                        Padding(padding: EdgeInsets.all(4),),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              color: Color(0xffe2e2e2),
                            ),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: new InputField(
                              hintText: 'Info',
                              obscureText: false,
                              textInputType: TextInputType.emailAddress,
                              textStyle: GoogleFonts.varelaRound(textStyle: textStyle),
                              hintStyle: hintStyle,
                              controller: infoController,
                              textCapitalization: TextCapitalization.none,
                              bottomMargin: 8.0,
                              validateFunction: validations.validateTextInput,
                              onSaved: (String value) {
                                user.info = value;
                              }),
                        ),
                        Padding(padding: EdgeInsets.all(8)),
                        Text(
                          'Cambio de contraseña',
                          style: GoogleFonts.varelaRound(textStyle: TextStyle(fontWeight: FontWeight.bold)),
                        ),
                        Padding(padding: EdgeInsets.all(4)),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              color: Color(0xffe2e2e2),
                            ),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: new PasswordInputField(
                            hintText: "Contraseña",
                            obscureText: true,
                            controller: oldPassController,
                            textInputType: TextInputType.text,
                            textStyle: GoogleFonts.varelaRound(textStyle: textStyle),
                            hintStyle: hintStyle,
                            bottomMargin: 1.0,
                            //validateFunction: validations.validatePassword,
                            onSaved: (String password) {

                            },
                          ),
                        ),
                        Padding(padding: EdgeInsets.all(4)),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              color: Color(0xffe2e2e2),
                            ),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: new PasswordInputField(
                            hintText: "Contraseña nueva",
                            obscureText: true,
                            controller: passwordController,
                            textInputType: TextInputType.text,
                            textStyle: GoogleFonts.varelaRound(textStyle: textStyle),
                            hintStyle: hintStyle,
                            bottomMargin: 1.0,
                            validateFunction: validations.validatePassword,
                            onSaved: (String password) {
                              this.password = password;
                            },
                          ),
                        ),
                        Padding(padding: EdgeInsets.all(4)),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              color: Color(0xffe2e2e2),
                            ),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: new PasswordInputField(
                            hintText: "Repita contraseña nueva",
                            obscureText: true,
                            controller: repeatPassController,
                            textInputType: TextInputType.text,
                            textStyle: GoogleFonts.varelaRound(textStyle: textStyle),
                            hintStyle: hintStyle,
                            bottomMargin: 1.0,
                            validateFunction: (value) =>
                                validations.validateRepeatPassword(
                                    value, passwordController.text),
                            onSaved: (String password) {},
                          ),
                        ),
                        Padding(padding: EdgeInsets.all(8)),
                        new RoundedButton(
                          buttonName: "Guardar",
                          onTap: handleFormSubmitted,
                          width: screenSize.width,
                          height: screenSize.height / 13,
                          bottomMargin: 10.0,
                          borderWidth: 0.0,
                          buttonColor: blueNexi,
                        ),
                      ].where((child) => child != null).toList(),
                    ),
                  ),
                ),
              ].where((child) => child != null).toList(),
            ),
          ),
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
      resizeToAvoidBottomPadding: true,
    );
  }

  void handleFormSubmitted() {
    final FormState form = formKey.currentState;
    if (!form.validate()) {
      autoValidate = true; // Start validating on every change.
      showInSnackBar('Por favor corrija los errores antes de continuar.');
    } else {
      form.save();
      updateProfile();
      //showMessageDialog();
      //onSelectedCountry();
      //doLogin();
    }
  }

  void showMessageDialog() {
    MyDialog.showNativePopUpWith2Buttons(
        context,
        'Informacion',
        'Ha ocurrido un problema y no se puede enviar el mensaje',
        'Cancelar',
        'Aceptar');
  }

  onSelectedCountry() async {
    if (country != null) {
      for (var c in countryList) {
        if (c.name == country) {
          setState(() {
            selectedCountry = c;
          });
          final prefs = await SharedPreferences.getInstance();
          prefs.setString('selectedCountry', json.encode(selectedCountry));
          prefs.setString(Global.keyApiUrl,
              selectedCountry.protocol + '://' + selectedCountry.url);
        }
      }
    } else {
      showInSnackBar("Debe seleccionar un pais");
    }
  }
}
