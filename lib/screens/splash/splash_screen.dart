import 'package:nexivoycomercios/screens/login/login_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nexivoycomercios/screens/main/pre_login_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:splashscreen/splashscreen.dart';

import '../../global.dart';
import '../../theme.dart';

class FirstScreen extends StatefulWidget {
  const FirstScreen({Key key}) : super(key: key);

  @override
  FirstScreenState createState() => new FirstScreenState();
}

class FirstScreenState extends State<FirstScreen> {
  bool isLogin = false;

  @override
  void initState() {
    checkUserLogin();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
      seconds: 5,
      navigateAfterSeconds: isLogin ? new LoginScreen() : new PreLoginScreen(),
      image: new Image.asset('assets/NEXI-VOY_logotipo_WEB_BLANCO.png'),
      imageBackground: AssetImage("assets/fondo.png"),
      //backgroundColor: primaryColor,
      styleTextUnderTheLoader: new TextStyle(),
      photoSize: 100.0,
      onClick: ()=>print("Flutter Egypt"),
      loaderColor: Colors.white,
    );
  }

  void checkUserLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.getBool('login') != null) {
      setState(() {
        isLogin = prefs.getBool('login');
      });
    }
  }
}