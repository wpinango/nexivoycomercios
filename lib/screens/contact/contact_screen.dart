import 'dart:convert';

import 'package:flutter_map/flutter_map.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:nexivoycomercios/components/TextFields/input_field.dart';
import 'package:nexivoycomercios/components/buttons/rounded_button.dart';
import 'package:nexivoycomercios/components/zoombuttons_plugin_option.dart';
import 'package:nexivoycomercios/dialogs/dialog.dart';
import 'package:nexivoycomercios/models/contact.dart';
import 'package:nexivoycomercios/models/login_response.dart';
import 'package:nexivoycomercios/models/masters.dart';
import 'package:nexivoycomercios/services/validations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:latlong/latlong.dart';

import '../../theme.dart';

class ContactScreen extends StatefulWidget {
  const ContactScreen({Key key}) : super(key: key);

  @override
  ContactScreenState createState() => new ContactScreenState();
}

class ContactScreenState extends State<ContactScreen> {
  BuildContext context;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isInAsyncCall = false;
  Validations validations = new Validations();
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  ScrollController scrollController = new ScrollController();
  bool autoValidate = false;
  var timeout = const Duration(seconds: 30);
  LoginResponse loginResponse;
  Masters masters;
  List<Contact> contacts = new List();
  LatLng latLng;
  var markers;

  @override
  void initState() {
    super.initState();
    getUserData();
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  void getUserData() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.get('selectedCountry') != null) {
      setState(() {
        loginResponse =
            LoginResponse.fromJson(json.decode(prefs.get('loginData')));
        masters =
            Masters.fromJson(json.decode(json.encode(loginResponse.masters)));
        for (var a in masters.contact) {
          Contact contact = new Contact();
          contact.schedule = a['horario'];
          contact.web = a['web'];
          contact.fax = a['fax'];
          contact.email = a['email'];
          contact.phone = a['phone'];
          contact.longitude = a['longitud'];
          contact.latitude = a['latitud'];
          contact.address = a['direccion'];
          contacts.add(contact);
        }
        if (contacts.length > 0) {
          latLng = new LatLng(double.parse(contacts.first.latitude), double.parse(contacts.first.longitude));
          markers = <Marker>[
            Marker(
              width: 100.0,
              height: 100.0,
              point: latLng,
              builder: (ctx) => Container(
                  child: Icon(
                    Icons.location_on,
                    color: Colors.redAccent,
                    size: 45,
                  )),
            ),
          ];
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    var height = AppBar().preferredSize.height;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: Container(
          height: height / 1.8,
          child: Image(
            image: AssetImage('assets/NEXI-VOY_logotipo_WEB_BLANCO.png'),
          ),
        ),
        centerTitle: true,
        iconTheme: new IconThemeData(color: Colors.white),
        actions: <Widget>[],
        backgroundColor: primaryColor,
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: ModalProgressHUD(
        child: SingleChildScrollView(
          controller: scrollController,
          child: new Container(
            //height: screenSize.height,
            width: screenSize.width,
            padding: new EdgeInsets.all(8.0),
            child: new Column(
              children: <Widget>[
                Container(
                  margin:EdgeInsets.all(8),
                  child: Text(
                    'Conctato',
                    style: GoogleFonts.varelaRound(textStyle: TextStyle(
                        fontSize: 18)),
                  ),
                ),
                Container(
                  padding: new EdgeInsets.all(7.0),
                  child: Form(
                    key: formKey,
                    autovalidate: autoValidate,
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              color: Color(0xffe2e2e2),
                            ),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: new InputField(
                              obscureText: false,
                              hintText: 'Nombre',
                              textInputType: TextInputType.emailAddress,
                              textStyle: GoogleFonts.varelaRound(textStyle: textStyle),
                              hintStyle: hintStyle,
                              textCapitalization: TextCapitalization.none,
                              //textFieldColor: textFieldColor,
                              bottomMargin: 1,
                              validateFunction: validations.validateTextInput,
                              onSaved: (String value) {
                                //user.name = value;
                              }),
                        ),
                       Padding(padding: EdgeInsets.all(4),),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              color: Color(0xffe2e2e2),
                            ),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: new InputField(
                              obscureText: false,
                              hintText: 'Email',
                              textInputType: TextInputType.emailAddress,
                              textStyle: GoogleFonts.varelaRound(textStyle: textStyle),
                              hintStyle: hintStyle,
                              textCapitalization: TextCapitalization.none,
                              //textFieldColor: textFieldColor,
                              bottomMargin: 1,
                              validateFunction: validations.validateTextInput,
                              onSaved: (String value) {
                                //user.lastName = value;
                              }),
                        ),
                        Padding(padding: EdgeInsets.all(4),),
                        /*new Text(
                          'Mensaje',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),*/
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              color: Color(0xffe2e2e2),
                            ),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: new InputField(
                              obscureText: false,
                              hintText: 'Mensaje',
                              maxLines: 3,
                              textInputType: TextInputType.emailAddress,
                              textStyle: GoogleFonts.varelaRound(textStyle: textStyle),
                              hintStyle: hintStyle,
                              textCapitalization: TextCapitalization.none,
                              //textFieldColor: textFieldColor,
                              bottomMargin: 1,
                              validateFunction: validations.validateTextInput,
                              onSaved: (String value) {
                                //user.email = value;
                              }),
                        ),
                        Padding(padding: EdgeInsets.all(8),),
                        new RoundedButton(
                          buttonName: "Enviar",
                          onTap: handleFormSubmitted,
                          width: screenSize.width,
                          height: screenSize.height / 13,
                          bottomMargin: 10.0,
                          borderWidth: 0.0,
                          buttonColor: blueNexi,
                        ),
                      ].where((child) => child != null).toList(),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                ),
                new Text(
                  'Donde estamos?',
                  style: GoogleFonts.varelaRound(textStyle: TextStyle(fontWeight: FontWeight.bold)),
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                ),
                Row(
                  children: <Widget>[
                    Icon(Icons.location_on),
                    Flexible(
                      child: Text(contacts[0].address, style: GoogleFonts.varelaRound(),),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                ),
                latLng != null?
                Container(
                  height: screenSize.height / 3,
                  child: FlutterMap(
                    options: MapOptions(
                        center: latLng,
                        zoom: 17.0,
                        plugins: [
                          ZoomButtonsPlugin(),
                        ],
                        onTap: (LatLng point) {

                        }),
                    layers: [
                      TileLayerOptions(
                        urlTemplate:
                        'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                        subdomains: ['a', 'b', 'c'],
                        tileProvider: NonCachingNetworkTileProvider(),
                      ),
                      MarkerLayerOptions(markers: markers),
                      ZoomButtonsPluginOption(
                          minZoom: 4,
                          maxZoom: 19,
                          mini: true,
                          padding: 10,
                          alignment: Alignment.bottomRight)
                    ],
                  ),
                ) : null,
                Row(
                  children: <Widget>[
                    Icon(Icons.phone),
                    //Flexible(child: Row(children: <Widget>[],),),
                    Flexible(
                      child: Text(contacts[0].phone != null
                          ? contacts[0].phone
                          : contacts[0].fax, style: GoogleFonts.varelaRound(),),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                ),
                Row(
                  children: <Widget>[
                    Icon(Icons.email),
                    Flexible(
                        child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(contacts[0].email, style: GoogleFonts.varelaRound(),),
                        Text(contacts[0].web, style: GoogleFonts.varelaRound(),)
                      ],
                    )),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.all(8),
                ),
                new Text(
                  'Horario',
                  style: GoogleFonts.varelaRound(textStyle: TextStyle(fontWeight: FontWeight.bold)),
                ),
                Row(
                  children: <Widget>[
                    Icon(Icons.schedule),
                    Flexible(
                      child: Text(contacts[0].schedule, style: GoogleFonts.varelaRound(),),
                    ),
                  ],
                ),
              ].where((child) => child != null).toList(),
            ),
          ),
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
      resizeToAvoidBottomPadding: false,
    );
  }

  void handleFormSubmitted() {
    final FormState form = formKey.currentState;
    if (!form.validate()) {
      autoValidate = true; // Start validating on every change.
      showInSnackBar('Por favor corrija los errores antes de continuar.');
    } else {
      form.save();
      showMessageDialog();
      //onSelectedCountry();
      //doLogin();
    }
  }

  void requestSendMessage() async {

  }

  void handleMessageResponse(var response) async {

  }

  void showMessageDialog() async {
    if ( await MyDialog.showNativePopUpWith2Buttons(
        context,
        'Informacion',
        'Envio del mensaje correcto',
        'Cancelar',
        'Aceptar')) {
      Navigator.of(context).pop();
    }
  }

  void cleanData() {

  }
}
