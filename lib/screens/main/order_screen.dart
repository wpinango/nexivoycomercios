import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';
import 'package:nexivoycomercios/components/buttons/rounded_button.dart';
import 'package:nexivoycomercios/components/profile_title.dart';
import 'package:nexivoycomercios/daos/order_dao.dart';
import 'package:nexivoycomercios/database/database_helper.dart';
import 'package:nexivoycomercios/dialogs/dialog.dart';
import 'package:nexivoycomercios/dialogs/order_dialog.dart';
import 'package:nexivoycomercios/global.dart';
import 'package:nexivoycomercios/models/business_delivery.dart';
import 'package:nexivoycomercios/models/country.dart';
import 'package:nexivoycomercios/models/delivery.dart';
import 'package:nexivoycomercios/models/login_response.dart';
import 'package:nexivoycomercios/models/order.dart';
import 'package:nexivoycomercios/screens/new_order_screen/new_order_screen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../../api_url.dart';
import '../../routes.dart';
import '../../theme.dart';

class OrderScreen extends StatefulWidget {
  const OrderScreen({Key key}) : super(key: key);

  @override
  OrderScreenState createState() => new OrderScreenState();
}

Future<dynamic> myBackgroundMessageHandler(Map<String, dynamic> message) {
  if (message.containsKey('data')) {
    // Handle data message
    final dynamic data = message['data'];
  }

  if (message.containsKey('notification')) {
    // Handle notification message
    final dynamic notification = message['notification'];
  }

  // Or do other work.
}

class OrderScreenState extends State<OrderScreen> {
  BuildContext context;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  List<String> values = ['Pedidos activos', 'Pedidos pendientes'];
  bool isInAsyncCall = false;
  String country = 'Pedidos activos';
  final dbHelper = DatabaseHelper.instance;
  List businessOrders = new List();
  List businessDeliveries = new List();
  int orderType = 1;
  var timeout = const Duration(seconds: 50);
  Country selectedCountry;
  LoginResponse loginResponse;
  List businessDeliveriesTemp = new List();
  FirebaseMessaging firebaseMessaging = new FirebaseMessaging();
  StreamSubscription iosSubscription;
  BusinessDelivery businessDelivery;
  Map freelance;
  OrderDao orderDao;
  List deliveredOrders;

  @override
  initState() {
    super.initState();
    initVars();
    getSelectedCountry();
    // firebaseMessaging.subscribeToTopic('news');
  }

  void getSelectedCountry() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.get('selectedCountry') != null) {
      setState(() {
        selectedCountry =
            Country.fromJson(json.decode(prefs.get('selectedCountry')));
      });
    }
    loginResponse = LoginResponse.fromJson(json.decode(prefs.get('loginData')));
  }

  initVars() async {
    orderDao = new OrderDao();
    businessOrders = new List();
    businessDeliveries = new List();
    businessDeliveriesTemp = new List();
    businessDelivery = new BusinessDelivery();
    businessDelivery.deliveries = new Map();
    deliveredOrders = await orderDao.getAllOrders();
    final allRows = await dbHelper.queryAllRows();
    allRows
        .forEach((row) => businessDeliveriesTemp.add((Delivery.fromJson(row))));
    setState(() {});
    requestOrders();
    final prefs = await SharedPreferences.getInstance();
    firebaseMessaging.configure(
      onBackgroundMessage:
      Platform.isAndroid ? myBackgroundMessageHandler : null,
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        String title = message['notification']['title'] ?? '';
        String content = message['notification']['body'] ?? '';
        MyDialog.showNativePopUpWith2Buttons(
            context, title, content, 'Cancelar', 'Aceptar');
        if (title == 'Nuevo pedido Nexivoy') {
          requestOrders();
        }
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        // TODO optional
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        // TODO optional
      },
    );
    firebaseMessaging.getToken().then((token) {
      prefs.setString('fbToken', token);
    });
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  void requestOrders() async {
    showProgressDialog();
    try {
      List references = new List();
      for (var a in businessDeliveriesTemp) {
        references.add(a.reference);
      }
      var body = {
        'deliveries': references,
        'token': loginResponse.token,
      };
      Map<String, String> map = {
        'datos': json.encode(body),
      };
      //print(map);
      Map<String, String> headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
      };
      final prefs = await SharedPreferences.getInstance();
      String apiVersion = prefs.get('apiVersion');
      final response = await http
          .post(
          selectedCountry.protocol +
              '://' +
              selectedCountry.url +
              ApiUrl.urlGetOrders.replaceAll('?', apiVersion),
          headers: headers,
          body: map)
          .timeout(timeout);
      //print(response.body);
      if (response.body != "") {
        handleRefreshData(response.body);
      } else {
        handleRefreshData("");
      }
    } catch (e) {
      handleRefreshData("");
      print('algo salio mal ' + e.toString());
    }
  }

  void handleRefreshData(final response) async {
    cancelProgressDialog();
    if (response != "") {
      Map<String, dynamic> map = json.decode(response);
      if (map['resultado']['code'] == 0) {
        //print(json.encode(map['resultado']));
        try {
          businessOrders.clear();
          businessDeliveries.clear();
          deliveredOrders.clear();
          deliveredOrders = await orderDao.getAllOrders();
          if (map['resultado'].containsKey('negocios')) {
            for (var a in map['resultado']['negocios']) {
              businessOrders.add(a);
            }
          }
          if (map['resultado'].containsKey('deliveries')) {
            for (var a in map['resultado']['deliveries']) {
              for (var b in businessDeliveriesTemp) {
                if (a['referencia'] == b.reference) {
                  if (a['estado'] == 1) {
                    businessDeliveries.add(b);
                  }
                }
              }
            }
          }
          businessDelivery.deliveries = new Map();
          List<Delivery> delivery = new List();
          for (var a in businessDeliveries) {
            if (businessDelivery.deliveries.containsKey(a.business)) {
              delivery.add(a);
              businessDelivery.deliveries[a.business] = delivery;
            } else {
              delivery = new List();
              delivery.add(a);
              businessDelivery.deliveries[a.business] = delivery;
            }
          }
          ApiUrl.setPushService();
          setState(() {});
        } catch (e) {
          print(e.toString());
        }
      } else {
        showInSnackBar('No se ha podido obtener el listado de pedidos.');
      }
    } else if (response == "httpExcep") {
      showInSnackBar("Revice la conexion a internet");
    } else {
      showInSnackBar("Algo salio mal");
    }
  }

  void requestSetOrder(int status, var order) async {
    showProgressDialog();
    try {
      var body = {
        'token': loginResponse.token,
        'id_pedido': order['id_pedido'],
        'id_estado': status,
      };
      if (status == 2) {
        body['observaciones'] = order['observaciones'];
      } else if (status == 5) {
        body['tiempo_preparacion'] = order['tiempo_preparacion'];
      }
      Map<String, String> map = {
        'datos': json.encode(body),
      };
      //print(map);
      Map<String, String> headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
      };
      final prefs = await SharedPreferences.getInstance();
      String apiVersion = prefs.get('apiVersion');
      final response = await http
          .post(
          selectedCountry.protocol +
              '://' +
              selectedCountry.url +
              ApiUrl.urlSetOrder.replaceAll('?', apiVersion),
          headers: headers,
          body: map)
          .timeout(timeout);
      //print(response.body);
      if (response.body != "") {
        handleOrderResponse(response.body);
      } else {
        handleOrderResponse("");
      }
    } catch (e) {
      handleOrderResponse("");
      print('algo salio mal ' + e.toString());
    }
  }

  void handleOrderResponse(var response) async {
    cancelProgressDialog();
    if (response != "") {
      Map<String, dynamic> map = json.decode(response);
      if (map['resultado']['code'] == 0) {
        //print(json.encode(map['resultado']));
        requestOrders();
      } else {
        showInSnackBar('Se ha producido un error al registrar su pedido.');
      }
    } else if (response == "httpExcep") {
      showInSnackBar("Revice la conexion a internet");
    } else {
      showInSnackBar("Algo salio mal");
    }
  }

  void requestTransportInfo(order, businessData, bool isDelivery) async {
    final prefs = await SharedPreferences.getInstance();
    showProgressDialog();
    var body = {
      'type': 0,
      // 1 - coordenadas solamente  0 - toda la informacion
      'id': order['id_pedido'],
      'token': loginResponse.token,
    };
    Map<String, String> map = {
      'datos': json.encode(body),
    };
    Map<String, String> headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    String apiVersion = prefs.get('apiVersion');
    try {
      final response = await http
          .post(
          selectedCountry.protocol +
              '://' +
              selectedCountry.url +
              ApiUrl.trackOrder.replaceAll('?', apiVersion),
          headers: headers,
          body: map)
          .timeout(timeout);
      if (response.body != "") {
        handleResponse(response.body, order, businessData, isDelivery);
      } else {
        handleResponse("", order, businessData, isDelivery);
      }
    } catch (e) {
      handleResponse("", order, businessData, isDelivery);
    }
  }

  void handleResponse(
      var responseBody, order, businessData, bool isDelivery) async {
    cancelProgressDialog();
    if (responseBody != "") {
      Map<String, dynamic> map = json.decode(responseBody);
      if (map['resultado']['code'] == 0) {
        freelance = map['resultado']['freelance'];
        //requestOrderDelivered(order, businessData);
        if (freelance != null && freelance['name'] != " ") {
          if (isDelivery) {
            requestOrderDelivered(order, businessData);
          } else {
            showTransportInfoDialog(freelance);
          }
        } else {
          MyDialog.showNativePopUpWithButton(context, 'Alerta',
              'Este pedido aun no tiene taxi asignado', 'Cerrar');
        }
        setState(() {});
      } else if (map['resultado']['code'] == -1) {
        showInSnackBar('No hay negocios en esta zona');
      } else if (map['resultado']['code'] == -3) {}
    } else {
      cancelProgressDialog();
      showInSnackBar('Algo salio mal');
    }
  }

  void requestOrderDelivered(var order, var businessData) async {
    showProgressDialog();
    DateTime d = new DateTime.now();
    double ts = d.microsecondsSinceEpoch / 1000;
    try {
      var body = {
        'token': loginResponse.token,
        'ts': ts.truncate(), //(la hora en segundos)
        'idPed': order['id_pedido'], //id del pedido
        'idNeg': businessData['id_negocio'], // id del negocio
        'state': 7 //id
      };

      Map<String, String> map = {
        'datos': json.encode(body),
      };
      //print(map);
      Map<String, String> headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
      };
      final prefs = await SharedPreferences.getInstance();
      String apiVersion = prefs.get('apiVersion');
      final response = await http
          .post(
          selectedCountry.protocol +
              '://' +
              selectedCountry.url +
              ApiUrl.urlSetOrderDelivered.replaceAll('?', apiVersion),
          headers: headers,
          body: map)
          .timeout(timeout);
      //print(response.body);
      if (response.body != "") {
        handleOrderDeliveredResponse(response.body, order);
      } else {
        handleOrderDeliveredResponse("", order);
      }
    } catch (e) {
      handleOrderDeliveredResponse("", order);
      print('algo salio mal ' + e.toString());
    }
  }

  handleOrderDeliveredResponse(var response, order) async {
    cancelProgressDialog();
    if (response != "") {
      Map<String, dynamic> map = json.decode(response);
      if (map['resultado']['code'] == 0) {
        orderDao.insertOrder(Order.fromJson(order));
        requestOrders();
      } else {
        showInSnackBar('Se ha producido un error al registrar su pedido.');
      }
    } else if (response == "httpExcep") {
      showInSnackBar("Revice la conexion a internet");
    } else {
      showInSnackBar("Algo salio mal");
    }
  }

  void doLogout() async {
    showProgressDialog();
    try {
      var body = {
        'token': loginResponse.token,
      };
      Map<String, String> map = {
        'datos': json.encode(body),
      };
      //print(map);
      Map<String, String> headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
      };
      final prefs = await SharedPreferences.getInstance();
      String apiVersion = prefs.get('apiVersion');
      final response = await http
          .post(
          selectedCountry.protocol +
              '://' +
              selectedCountry.url +
              ApiUrl.urlLogout.replaceAll('?', apiVersion),
          headers: headers,
          body: map)
          .timeout(timeout);
      //print(response.body);
      if (response.body != "") {
        handleLogoutResponse(response.body);
      } else {
        handleLogoutResponse("");
      }
    } catch (e) {
      handleLogoutResponse("");
      print('algo salio mal ' + e.toString());
    }
  }

  void handleLogoutResponse(final response) async {
    cancelProgressDialog();
    if (response != "") {
      Map<String, dynamic> map = json.decode(response);
      if (map['resultado']['code'] == 0) {
        //print(json.encode(map['resultado']));
        try {
          final prefs = await SharedPreferences.getInstance();
          prefs.setString(Global.keyCredentials, json.encode(''));
          prefs.setString('loginData', json.encode(''));
          //print(orderDao.getAllOrders());
          prefs.setBool('login', false);
          for (var a in businessDeliveriesTemp) {
            dbHelper.delete(a.reference);
          }
          Routes.replacementScreen(context, '/Login');
          setState(() {});
        } catch (e) {
          print(e.toString());
        }
      } else if (map['resultado']['code'] == -1) {
        showInSnackBar('El usuario o contraseña son incorrectos');
      } else if (map['resultado']['code'] == -2) {
        showInSnackBar('Servicio no disponible');
      }
    } else if (response == "httpExcep") {
      showInSnackBar("Revice la conexion a internet");
    } else {
      showInSnackBar("Algo salio mal");
    }
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    var height = AppBar().preferredSize.height;
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        iconTheme: new IconThemeData(color: Colors.white),
        title: Container(
          height: height / 1.8,
          child: Image(
            image: AssetImage('assets/NEXI-VOY_logotipo_WEB_BLANCO.png'),
          ),
        ),
        centerTitle: true,
        actions: <Widget>[],
        backgroundColor: primaryColor,
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: ModalProgressHUD(
        child: SingleChildScrollView(
          child: new Container(
            height: screenSize.height,
            width: screenSize.width,
            padding: new EdgeInsets.all(8.0),
            child: new Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Mis pedidos',
                      style: GoogleFonts.varelaRound(
                          textStyle: TextStyle(fontSize: 18)),
                    ),
                    IconButton(
                      icon: Icon(Icons.refresh),
                      onPressed: () {
                        requestOrders();
                      },
                    ),
                  ],
                ),
                Theme(
                    data: new ThemeData(
                        canvasColor: blueNexi,
                        primaryColor: blueNexi,
                        accentColor: blueNexi,
                        hintColor: blueNexi),
                    child: Container(
                        height: screenSize.height / 13,
                        width: screenSize.width / 1.05,
                        padding: EdgeInsets.symmetric(horizontal: 8.0),
                        decoration: BoxDecoration(
                          color: blueNexi,
                          borderRadius: BorderRadius.circular(5.0),
                          border: Border.all(
                              color: white,
                              style: BorderStyle.solid,
                              width: 0.80),
                        ),
                        child: new DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                              items: values.map<DropdownMenuItem<String>>(
                                      (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(
                                        value,
                                        style: GoogleFonts.varelaRound(
                                            textStyle: TextStyle(color: white)),
                                      ),
                                    );
                                  }).toList(),
                              onChanged: (String value) {
                                setState(() {
                                  country = value;
                                  if (value == 'Pedidos activos') {
                                    orderType = 1;
                                  } else if (value == 'Pedidos pendientes') {
                                    orderType = 2;
                                  }
                                });
                              },
                              value: country),
                        ))),
                orderType == 1
                    ? Container(
                  height: screenSize.height / 1.5,
                  child: new ListView.builder(
                    itemCount: businessOrders.length,
                    itemBuilder: (BuildContext context, int index) {
                      var business = businessOrders[index];
                      for (var deliveredOrder in deliveredOrders) {
                        businessOrders[index]['pedidos'].removeWhere(
                                (element) =>
                            element['referencia'] ==
                                deliveredOrder.reference);
                      }
                      return Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                            color: Color(0xffe2e2e2),
                          ),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        width: screenSize.width / 1.5,
                        child: new ExpansionTile(
                          backgroundColor: white,
                          title: new Text(
                            business['negocio'],
                            style: new TextStyle(
                              fontSize: 14.0,
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.italic,
                            ),
                          ),
                          children: businessOrders[index]['pedidos']
                              .map<Widget>((data) => getBusinessOrders(
                              index,
                              data,
                              screenSize,
                              businessOrders[index]))
                              .toList(),
                        ),
                      );
                    },
                  ),
                )
                    : Container(
                  height: screenSize.height / 1.5,
                  child: new ListView.builder(
                    itemCount: businessDelivery.deliveries.length != null
                        ? businessDelivery.deliveries.length
                        : 0,
                    itemBuilder: (context, i) {
                      var list =
                      businessDelivery.deliveries.keys.toList();
                      return Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          border: Border.all(
                            color: Color(0xffe2e2e2),
                          ),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: new ExpansionTile(
                          backgroundColor: white,
                          title: new Text(
                            list[i],
                            style: new TextStyle(
                              fontSize: 14.0,
                              fontWeight: FontWeight.bold,
                              fontStyle: FontStyle.italic,
                            ),
                          ),
                          children: <Widget>[
                            new Column(
                              children: buildExpandableContent(
                                  businessDelivery, screenSize),
                            ),
                          ].where((child) => child != null).toList(),
                        ),
                      );
                    },
                  ),
                ),
              ].where((child) => child != null).toList(),
            ),
          ),
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
      drawer: new Theme(
          data: Theme.of(context).copyWith(
            canvasColor: primaryColor,
          ),
          child: Container(
            width: screenSize.width / 3,
            child: new Drawer(
              child: new ListView(
                children: <Widget>[
                  /*GestureDetector(
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.all(8),
                          ),
                          Icon(
                            Icons.send,
                            color: white,
                            size: 40,
                          ),
                          Text(
                            'Enviar pedido',
                            style: GoogleFonts.varelaRound(
                                textStyle: TextStyle(color: white)),
                          )
                        ],
                      ),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                      openNewOrderScreen();
                    },
                  ),*/
                  Divider(
                    color: white,
                  ),
                  Padding(
                    padding: EdgeInsets.all(4),
                  ),
                  GestureDetector(
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.account_balance,
                            color: white,
                            size: 40,
                          ),
                          Text(
                            'Negocios',
                            style: GoogleFonts.varelaRound(
                                textStyle: TextStyle(color: white)),
                          )
                        ],
                      ),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                      Routes.showModalScreen(context, '/Business');
                    },
                  ),
                  Divider(
                    color: white,
                  ),
                  Padding(
                    padding: EdgeInsets.all(4),
                  ),
                  GestureDetector(
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.person,
                            color: white,
                            size: 40,
                          ),
                          Text(
                            'Mi cuenta',
                            style: GoogleFonts.varelaRound(
                                textStyle: TextStyle(color: white)),
                          )
                        ],
                      ),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                      Routes.showModalScreen(context, '/Account');
                    },
                  ),
                  Divider(
                    color: white,
                  ),
                  Padding(
                    padding: EdgeInsets.all(4),
                  ),
                  GestureDetector(
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.phone,
                            color: white,
                            size: 40,
                          ),
                          Text(
                            'Contacto',
                            style: GoogleFonts.varelaRound(
                                textStyle: TextStyle(color: white)),
                          )
                        ],
                      ),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                      Routes.showModalScreen(context, '/Contact');
                    },
                  ),
                  Divider(
                    color: white,
                  ),
                  Padding(
                    padding: EdgeInsets.all(4),
                  ),
                  GestureDetector(
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          Icon(
                            Icons.exit_to_app,
                            color: white,
                            size: 40,
                          ),
                          Text(
                            'Cerrar sesion',
                            style: GoogleFonts.varelaRound(
                                textStyle: TextStyle(color: white)),
                          )
                        ],
                      ),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                      closeSessionDialog();
                    },
                  ),
                  Divider(
                    color: white,
                  ),
                ],
              ),
            ),
          )),
      resizeToAvoidBottomPadding: false,
    );
  }

  buildExpandableContent(BusinessDelivery businessDelivery, Size screenSize) {
    List<Widget> columnContent = [];
    for (var a in businessDelivery.deliveries.entries) {
      for (Delivery d in a.value) {
        Uint8List decoded =
        base64Decode(d.ticket.split('data:image/jpeg;base64,')[1]);
        columnContent.add(
          Container(
            decoration: BoxDecoration(color: Color.fromRGBO(238, 238, 238, 1)),
            child: Column(
              children: <Widget>[
                Container(
                    child: Text('Ref.' + d.reference),
                    padding: EdgeInsets.all(16),
                    alignment: Alignment.centerLeft),
                Container(
                  decoration:
                  BoxDecoration(color: Color.fromRGBO(251, 222, 204, 1)),
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Text('Observaciones: ' + d.observations),
                        padding: EdgeInsets.all(16),
                        alignment: Alignment.centerLeft,
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: Image.memory(decoded),
                        width: screenSize.width,
                        height: screenSize.height / 2.5,
                        padding: EdgeInsets.all(8),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      }
    }
    return columnContent;
  }

  void openNewOrderScreen() async {
    if (await Navigator.push(context,
        new MaterialPageRoute(builder: (context) => new NewOrderScreen()))) {
      initVars();
    }
  }

  void closeSessionDialog() async {
    if (await MyDialog.showNativePopUpWith2Buttons(
        context, "Alerta", 'Desear cerrar sesion?', 'Cancelar', "Aceptar")) {
      doLogout();
    }
  }

  Widget getBusinessOrders(int index, var order, Size size, var businessData) {
    var format = Global.getCurrencySymbol(selectedCountry);
    List<Widget> widgets = new List();
    widgets.add(Container(
      margin: EdgeInsets.all(4),
      height: size.height / 20,
      decoration: BoxDecoration(
        color: textFieldColor,
        borderRadius: BorderRadius.circular(5.0),
        border: Border.all(color: white, style: BorderStyle.solid, width: 0.80),
      ),
      width: size.width / 1.1,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            order['fecha'].toString().split(" ")[0] + ' ',
            style: GoogleFonts.varelaRound(fontSize: 13),
          ),
          Text(order['fecha'].toString().split(" ")[1] + ' ',
              style: GoogleFonts.varelaRound(fontSize: 13)),
          Text('Ref.' + order['referencia'],
              style: GoogleFonts.varelaRound(
                  fontWeight: FontWeight.bold, fontSize: 13)),
        ],
      ),
    ));
    widgets.add(Container(
      margin: EdgeInsets.all(8),
      width: size.width / 1.4,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Importe ' + format.format(double.parse(order['total'])),
            style: GoogleFonts.varelaRound(),
          ),
          Text('Tiempo de preparacion ' + order['tiempo_preparacion'],
              style: GoogleFonts.varelaRound())
        ],
      ),
    ));
    widgets.add(Container(
      width: size.width / 1.4,
      child: Divider(),
    ));
    for (var product in order['productos']) {
      widgets.add(Text(
          '-' +
              product['cantidad'] +
              'x ' +
              product['categoria'] +
              '->' +
              product['producto'],
          style: GoogleFonts.varelaRound()));
      if (isCustom(product)) {
        widgets.add(getCustomWidget(product, size));
      }
    }
    widgets.add(Padding(
      padding: EdgeInsets.all(4),
    ));
    if (order['id_estado_gestion'] != '4' &&
        order['id_estado_gestion'] != '5') {
      widgets.add(Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              new RoundedButton(
                buttonName: "Rechazar",
                onTap: () {
                  showOrderDialog(2, order);
                },
                width: size.width / 4,
                height: size.height / 13,
                bottomMargin: 10.0,
                borderWidth: 0.0,
                buttonColor: blueNexi,
              ),
              new RoundedButton(
                buttonName: "Confirmar",
                onTap: () {
                  showOrderDialog(4, order);
                },
                width: size.width / 4,
                height: size.height / 13,
                bottomMargin: 10.0,
                borderWidth: 0.0,
                buttonColor: blueNexi,
              ),
            ],
          ),
        ],
      ));
    } else if (order['id_estado_gestion'] == '4' ||
        order['id_estado_gestion'] == '5') {
      widgets.add(Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              new RoundedButton(
                buttonName: "Entregado",
                onTap: () {
                  showConfirmDialog(order, businessData);
                },
                width: size.width / 4,
                height: size.height / 13,
                bottomMargin: 10.0,
                borderWidth: 0.0,
                buttonColor: blueNexi,
              ),
              new RoundedButton(
                buttonName: "NexiTaxi",
                onTap: () {
                  requestTransportInfo(order, businessData, false);
                },
                width: size.width / 4,
                height: size.height / 13,
                bottomMargin: 10.0,
                borderWidth: 0.0,
                buttonColor: blueNexi,
              ),
            ],
          ),
        ],
      ));
    }
    widgets.add(Padding(
      padding: EdgeInsets.all(8),
    ));
    return Column(
      children: widgets,
    );
  }

  void showConfirmDialog(order, businessData) async {
    if (await MyDialog.showNativePopUpWith2Buttons(context, 'Confirmar entrega',
        '¿Desea confirmar la entrega del pedido?', 'Cerrar', 'Confirmar')) {
      requestTransportInfo(order, businessData, true);
    }
  }

  void showOrderDialog(var status, var o) async {
    Map result = await Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => new OrderDialog(
              order: o,
              status: status,
              selectedCountry: selectedCountry,
            )));
    if (result != null) {
      if (result.containsKey('cambio_horario')) {
        DateTime dateTime1 =
        DateFormat('HH:mm:ss').parse(result['tiempo_preparacion']);
        DateTime dateTime2 =
        DateFormat('HH:mm:ss').parse(result['cambio_horario']);
        if (dateTime1 != dateTime2) {
          status = 5;
          result['tiempo_preparacion'] = result['cambio_horario'];
        }
      } else if (result.containsKey('motivo_rechazo')) {
        result['obervaciones'] = result['motivo_rechazo'];
      }
      requestSetOrder(status, result);
    }
  }

  bool isCustom(var product) {
    bool isCustom = false;
    List ingredients = product['ingredientes'] ?? [];
    List combinations = product['combinaciones'] ?? [];
    List extras = product['extras'] ?? [];
    if (ingredients.isNotEmpty ||
        combinations.isNotEmpty ||
        extras.isNotEmpty) {
      isCustom = true;
    }
    return isCustom;
  }

  Widget getCustomWidget(var product, Size size) {
    List ingredients = product['ingredientes'] ?? [];
    List combinations = product['combinaciones'] ?? [];
    List extras = product['extras'] ?? [];
    List<Widget> widgets = new List();
    if (ingredients.isNotEmpty) {
      widgets.add(Row(
        children: [
          Text(
            'No quiero: ',
            style: GoogleFonts.varelaRound(fontWeight: FontWeight.bold),
            maxLines: 3,
          ),
          for (int i = 0; i < ingredients.length; i++)
            Text(ingredients.length == (i + 1)
                ? ingredients[i][1]
                : ingredients[i][1] + ',')
        ],
      ));
    }
    if (combinations.isNotEmpty) {
      widgets.add(Row(
        children: [
          Text(
            'Combinaciones: ',
            style: GoogleFonts.varelaRound(fontWeight: FontWeight.bold),
          ),
          for (int i = 0; i < combinations.length; i++)
            Text(combinations.length == (i + 1)
                ? combinations[i][1]
                : combinations[i][1] + ',')
        ],
      ));
    }
    if (extras.isNotEmpty) {
      widgets.add(Row(
        children: [
          Text(
            'Extras: ',
            style: GoogleFonts.varelaRound(fontWeight: FontWeight.bold),
          ),
          for (int i = 0; i < extras.length; i++)
            Text(extras.length == (i + 1) ? extras[i][1] : extras[i][1] + ',')
        ],
      ));
    }
    return Container(
      margin: EdgeInsets.all(2),
      width: size.width / 1.3,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: widgets,
      ),
    );
  }

  showTransportInfoDialog(Map freelance) async {
    try {
      Uint8List decoded;
      if (freelance.containsKey('avatar') &&
          freelance['avatar'] != "" &&
          freelance['avatar'] != " " &&
          freelance['avatar'] != null) {
        decoded =
            base64Decode(freelance['avatar'].toString().split('base64,')[1]);
      }
      showDialog(
          context: context,
          barrierDismissible: true,
          builder: (context) => Center(
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    padding: const EdgeInsets.all(16.0),
                    child: Material(
                      clipBehavior: Clip.antiAlias,
                      elevation: 2.0,
                      borderRadius: BorderRadius.circular(4.0),
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            ProfileTile(
                              title: "Ficha del taxista",
                              textColor: primaryColor,
                              subtitle: "",
                            ),
                            if (freelance.containsKey('avatar') &&
                                freelance['avatar'] != "" &&
                                freelance['avatar'] != null)
                              Container(
                                alignment: Alignment.center,
                                child: Image.memory(decoded),
                                width: 150,
                                height: 150,
                                padding: EdgeInsets.all(8),
                              ),
                            ListTile(
                                title: Text('Nombre'),
                                subtitle: Text(
                                  freelance['name'] != " "
                                      ? freelance['name']
                                      : "Sin asingar",
                                )),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: 120,
                                  child: ListTile(
                                      title: Text('Marca'),
                                      subtitle: Text(freelance['brand'] ??
                                          "Sin asingar")),
                                ),
                                Container(
                                  width: 120,
                                  child: ListTile(
                                      title: Text('Modelo'),
                                      subtitle: Text(
                                        freelance['model'] ?? "Sin asingar",
                                      )),
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: 120,
                                  child: ListTile(
                                      title: Text('Color'),
                                      subtitle: Text(
                                        freelance['color'] ?? "Sin asingar",
                                      )),
                                ),
                                Container(
                                  width: 120,
                                  child: ListTile(
                                      title: Text('Placa'),
                                      subtitle: Text(
                                        freelance['plate'] ?? "Sin asingar",
                                      )),
                                ),
                              ],),
                            /* if (freelance.containsKey('brand') &&
                                    freelance['brand'] != "")
                                  ListTile(
                                      title: Text('Marca'),
                                      subtitle: Text(
                                          freelance['brand'] ?? "Sin asingar")),*/
                            /*if (freelance.containsKey('model') &&
                                    freelance['model'] != "")
                                  ListTile(
                                      title: Text('Modelo'),
                                      subtitle: Text(
                                        freelance['model'] ?? "Sin asingar",
                                      )),*/
                            /*if (freelance.containsKey('color') &&
                                    freelance['color'] != "")
                                  ListTile(
                                      title: Text('Color'),
                                      subtitle: Text(
                                        freelance['color'] ?? "Sin asingar",
                                      )),
                                if (freelance.containsKey('plate') &&
                                    freelance['plate'] != "")
                                  ListTile(
                                      title: Text('Placa'),
                                      subtitle: Text(
                                        freelance['plate'] ?? "Sin asingar",
                                      )),*/
                          ],
                        ),
                      ),
                    ),
                  ),
                  FloatingActionButton(
                    backgroundColor: Colors.black,
                    child: Icon(
                      Icons.clear,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.pop(context, true);
                      //onWillPop1();
                    },
                  )
                ],
              ),
            ),
          ));
    } catch (e) {
      e.toString();
    }
  }
}