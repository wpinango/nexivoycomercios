import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nexivoycomercios/components/buttons/rounded_button.dart';
import 'package:nexivoycomercios/theme.dart';

import '../../routes.dart';

class PreLoginScreen extends StatefulWidget {
  const PreLoginScreen({Key key}) : super(key: key);

  @override
  PreLoginScreenState createState() => new PreLoginScreenState();
}


class PreLoginScreenState extends State<PreLoginScreen> {
  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        width: screenSize.width,
        height: screenSize.height,
        decoration: BoxDecoration(
          image: DecorationImage(image: AssetImage("assets/fondo.png"), fit: BoxFit.cover),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Padding(padding: EdgeInsets.all(24),),
            Container(
              child: Image.asset('assets/NEXI-VOY_logotipo_WEB_BLANCO.png'),
              width: screenSize.width / 1.5,
            ),
            new Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new RoundedButton(
                  buttonName: "Entrar",
                  onTap: handleFormSubmitted,
                  width: screenSize.width / 1.5,
                  height: screenSize.height / 12,
                  bottomMargin: 10.0,
                  borderWidth: 0.0,
                  buttonColor: blueNexi,
                ),
                Padding(padding: EdgeInsets.all(20),),
              ],),
          ],
        ),
      ),
    );
  }

  handleFormSubmitted() {
    Routes.replacementScreen(context, '/Login');
  }
}
