import 'dart:convert';

import 'package:google_fonts/google_fonts.dart';
import 'package:nexivoycomercios/components/TextFields/input_field.dart';
import 'package:nexivoycomercios/components/buttons/rounded_button.dart';
import 'package:nexivoycomercios/database/database_helper.dart';
import 'package:nexivoycomercios/dialogs/dialog.dart';
import 'package:nexivoycomercios/models/business.dart';
import 'package:nexivoycomercios/models/country.dart';
import 'package:nexivoycomercios/models/login_response.dart';
import 'package:nexivoycomercios/services/validations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../../api_url.dart';
import '../../theme.dart';

class NewOrderScreen extends StatefulWidget {
  const NewOrderScreen({Key key}) : super(key: key);

  @override
  NewOrderScreenState createState() => new NewOrderScreenState();
}

class NewOrderScreenState extends State<NewOrderScreen> {
  BuildContext context;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  List<String> values = [];
  bool isInAsyncCall = false;
  Business business;
  bool isImage = false;
  var image;
  List<Business> businesses = new List();
  LoginResponse loginResponse;
  Validations validations = new Validations();
  String comment;
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  bool autoValidate = false;
  var timeout = const Duration(seconds: 80);
  Country selectedCountry;
  final dbHelper = DatabaseHelper.instance;
  TextEditingController controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    getBusiness();
    getSelectedCountry();
  }

  void getBusiness() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.get('selectedCountry') != null) {
      setState(() {
        loginResponse =
            LoginResponse.fromJson(json.decode(prefs.get('loginData')));
        for (var a in loginResponse.business) {
          Business business = new Business();
          business.name = a['nombre'];
          business.info = a['info'];
          business.address = a['address'];
          business.addressId = a['id_direccion'];
          business.social = a['razon_social'];
          business.nif = a['nif'];
          business.phoneMobile = a['movil'];
          business.phone = a['telefono'];
          business.businessId = a['id_negocio'];
          businesses.add(business);
        }
      });
    }
  }

  void getSelectedCountry() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.get('selectedCountry') != null) {
      setState(() {
        selectedCountry =
            Country.fromJson(json.decode(prefs.get('selectedCountry')));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    var height = AppBar().preferredSize.height;
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        iconTheme: new IconThemeData(color: Colors.white),
        title: Container(
          height: height / 1.8,
          child: Image(
            image: AssetImage('assets/NEXI-VOY_logotipo_WEB_BLANCO.png'),
          ),
        ),
        centerTitle: true,
        actions: <Widget>[],
        backgroundColor: primaryColor,
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: ModalProgressHUD(
        child: new Container(
          height: screenSize.height,
          width: screenSize.width,
          padding: new EdgeInsets.all(8.0),
          child: new Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Nuevo pedido',
                    style: GoogleFonts.varelaRound(
                        textStyle: TextStyle(
                            //color: primaryColor,
                            //fontWeight: FontWeight.bold,
                            fontSize: 18)),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.all(8),
              ),
              Theme(
                  data: new ThemeData(
                      canvasColor: blueNexi,
                      primaryColor: blueNexi,
                      accentColor: blueNexi,
                      hintColor: blueNexi),
                  child: Container(
                      height: screenSize.height / 13,
                      width: screenSize.width / 1.05,
                      padding: EdgeInsets.symmetric(horizontal: 8.0),
                      decoration: BoxDecoration(
                        color: blueNexi,
                        borderRadius: BorderRadius.circular(5.0),
                        border: Border.all(
                            color: white,
                            style: BorderStyle.solid,
                            width: 0.80),
                      ),
                      child: new DropdownButtonHideUnderline(
                        child: DropdownButton<Business>(
                            isExpanded: true,
                            hint: Text(
                              'Seleccione un negocio de recogida',
                              style: GoogleFonts.varelaRound(
                                  textStyle:
                                      TextStyle(color: white, fontSize: 14)),
                            ),
                            items: businesses.map<DropdownMenuItem<Business>>(
                                (Business value) {
                              return DropdownMenuItem<Business>(
                                value: value,
                                child: Text(
                                  value.name,
                                  style: TextStyle(color: white),
                                ),
                              );
                            }).toList(),
                            onChanged: (Business value) {
                              setState(() {
                                business = value;
                              });
                            },
                            value: business),
                      ))),
              Container(
                margin: EdgeInsets.all(8),
                padding: EdgeInsets.only(left: 8, top: 8, right: 8),
                height: screenSize.height / 4,
                width: screenSize.width,
                decoration: BoxDecoration(
                  border: Border.all(
                    width: 0.5, //
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Observaciones:',
                      style: GoogleFonts.varelaRound(),
                    ),
                    Container(
                      /*decoration: BoxDecoration(
                        border: Border.all(
                          width: 0.5,
                        ),
                      ),*/
                      padding: new EdgeInsets.all(7.0),
                      child: new ConstrainedBox(
                        constraints: new BoxConstraints(
                          minWidth: screenSize.width / 1.8,
                          maxWidth: screenSize.width / 1.2,
                          minHeight: screenSize.height / 25,
                          maxHeight: screenSize.height / 14,
                        ),
                        child: new SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          reverse: true,
                          child: Form(
                            key: formKey,
                            child: new InputField(
                                hintText: 'Escribe aqui lo que deseas pedir',
                                obscureText: false,
                                textInputType: TextInputType.emailAddress,
                                textStyle: GoogleFonts.varelaRound(),
                                hintStyle: hintStyle,
                                controller: controller,
                                textCapitalization: TextCapitalization.none,
                                textFieldColor: textFieldColor,
                                bottomMargin: 8.0,
                                validateFunction: validations.validateTextInput,
                                onSaved: (String value) {
                                  comment = value;
                                }),
                          ), /*new TextField(
                            keyboardType: TextInputType.multiline,
                            maxLines: null,
                            //onSubmitted: currentIsComposing ? _handleSubmitted : null,
                            decoration: new InputDecoration.collapsed(
                              hintText: 'Escribe aqui lo que deseas pedir',
                            ),
                          ),*/
                        ),
                      ),
                    ),
                    new RoundedButton(
                      buttonName: "Añadir ticket",
                      onTap: settingModalBottomSheet,
                      width: screenSize.width,
                      height: screenSize.height / 14,
                      bottomMargin: 10.0,
                      borderWidth: 0.0,
                      buttonColor: blueNexi,
                    ),
                  ],
                ),
              ),
              isImage
                  ? Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(4),
                        ),
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Image.file(image),
                          width: screenSize.width / 5,
                          height: screenSize.height / 5,
                        ),
                        Padding(
                          padding: EdgeInsets.all(8),
                        ),
                        new RoundedButton(
                          buttonName: "Eliminar",
                          onTap: deleteImage,
                          width: screenSize.width / 2.5,
                          height: screenSize.height / 13,
                          bottomMargin: 10.0,
                          borderWidth: 0.0,
                          buttonColor: blueNexi,
                        ),
                      ],
                    )
                  : null,
              new RoundedButton(
                buttonName: "Confirmar pedido",
                onTap: handleFormSubmitted,
                width: screenSize.width,
                height: screenSize.height / 13,
                bottomMargin: 10.0,
                borderWidth: 0.0,
                buttonColor: blueNexi,
              ),
            ].where((child) => child != null).toList(),
          ),
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
      resizeToAvoidBottomPadding: false,
    );
  }

  void openCamera() async {
    image = await ImagePicker.pickImage(source: ImageSource.camera);
    print('imagen ' + image.toString());
    if (image != null) {
      isImage = true;
      setState(() {});
    }
  }

  void deleteImage() {
    isImage = false;
    setState(() {});
  }

  void handleFormSubmitted() {
    final FormState form = formKey.currentState;
    if (!form.validate() && !isImage) {
      autoValidate = true; // Start validating on every change.
      showInSnackBar('Por favor corrija los errores antes de continuar.');
    } else {
      form.save();
      requestGenerateTicket();
      //onSelectedCountry();
      //doLogin();
    }
  }

  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }

  void requestGenerateTicket() async {
    showProgressDialog();
    String reference = makeIdReference();
    try {
      var order = {
        'token': loginResponse.token,
        'referencia': reference,
        'observaciones': comment,
        'id_direccion_rec': business.addressId,
        'ticket':
            'data:image/jpeg;base64,' + base64Encode(image.readAsBytesSync()),
      };
      /*var delivery = {
        'referencia': reference,
        'ticket': 'data:image/jpeg;base64,' + base64Encode(image.readAsBytesSync()),
        'observaciones': comment,
        'id_negocio': business.addressId,
        'negocio': business.name,
      };*/
      var delivery = {
        DatabaseHelper.columnReference: reference,
        DatabaseHelper.columnTicket:
            'data:image/jpeg;base64,' + base64Encode(image.readAsBytesSync()),
        DatabaseHelper.columnObservations: comment,
        DatabaseHelper.columnBusinessId: business.addressId,
        DatabaseHelper.columnBusiness: business.name,
      };
      dbHelper.insert(delivery);
      //print(json.encode(dbHelper.queryAllRows()));
      Map<String, String> map = {
        'datos': json.encode(order),
      };
      print(map);
      Map<String, String> headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
      };
      final prefs = await SharedPreferences.getInstance();
      String apiVersion = prefs.get('apiVersion');
      final response = await http
          .post(
              selectedCountry.protocol +
                  '://' +
                  selectedCountry.url +
                  ApiUrl.urlSetTicket.replaceAll('?', apiVersion),
              headers: headers,
              body: map)
          .timeout(timeout);
      print(response.body);
      if (response.body != "") {
        handleResponseGenerateTicket(response.body);
      } else {
        handleResponseGenerateTicket("");
      }
    } catch (e) {
      handleResponseGenerateTicket("");
      print('algo salio mal ' + e.toString());
    }
  }

  void handleResponseGenerateTicket(var response) async {
    cancelProgressDialog();
    if (response != "") {
      Map<String, dynamic> map = json.decode(response);
      if (map['resultado']['code'] == 0) {
        print(json.encode(map['resultado']));
        try {
          cleanData();
          if (await MyDialog.showNativePopUpWithButton(context, 'Informacion',
              'El pedido se creo correctamente', 'Aceptar')) {
            Navigator.pop(context, true);
          }
        } catch (e) {
          print(e.toString());
        }
      } else {
        showInSnackBar('Se ha producido un error al registrar su pedido.');
      }
    } else if (response == "httpExcep") {
      showInSnackBar("Revice la conexion a internet");
    } else {
      showInSnackBar("Algo salio mal");
    }
  }

  String makeIdReference() {
    var fullDate = new DateTime.now();
    var year = fullDate.year.toString();
    var month = fullDate.month.toString();
    if ((fullDate.month + 1) < 10) month = '0' + fullDate.month.toString();
    var dia = fullDate.day.toString();
    if ((fullDate.day) < 10) dia = '0' + fullDate.day.toString();
    var hora = fullDate.hour.toString();
    if ((fullDate.hour) < 10) hora = '0' + fullDate.hour.toString();
    var minutes = fullDate.minute.toString();
    if ((fullDate.minute) < 10) minutes = '0' + fullDate.minute.toString();
    var seconds = fullDate.second.toString();
    if ((fullDate.second) < 10) seconds = '0' + fullDate.second.toString();
    year = year.substring(2, 4);
    return year +
        '' +
        month +
        '' +
        dia +
        '' +
        hora +
        '' +
        minutes +
        '' +
        seconds;
  }

  void cleanData() {
    isImage = false;
    image = null;
    comment = '';
    setState(() {});
  }

  void settingModalBottomSheet() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(Icons.camera_alt),
                    title: new Text('Camara'),
                    onTap: () {
                      openProviderImage(ImageSource.camera);
                      Navigator.pop(context);
                    }),
                new ListTile(
                  leading: new Icon(Icons.toys),
                  title: new Text('Galeria'),
                  onTap: () {
                    openProviderImage(ImageSource.gallery);
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
          );
        });
  }

  void openProviderImage(ImageSource type) async {
    image = await ImagePicker.pickImage(source: type);
    print('imagen ' + image.toString());
    if (image != null) {
      isImage = true;
      setState(() {});
    }
  }
}
