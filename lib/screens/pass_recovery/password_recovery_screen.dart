import 'dart:convert';

import 'package:google_fonts/google_fonts.dart';
import 'package:nexivoycomercios/components/TextFields/input_field.dart';
import 'package:nexivoycomercios/components/buttons/rounded_button.dart';
import 'package:nexivoycomercios/dialogs/dialog.dart';
import 'package:nexivoycomercios/services/validations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:native_widgets/native_widgets.dart';
import 'package:http/http.dart' as http;

import '../../api_url.dart';
import '../../theme.dart';

class PasswordRecoveryScreen extends StatefulWidget {
  const PasswordRecoveryScreen({Key key}) : super(key: key);

  @override
  PasswordRecoveryScreenState createState() => new PasswordRecoveryScreenState();
}

class PasswordRecoveryScreenState extends State<PasswordRecoveryScreen> {
  BuildContext context;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isInAsyncCall = false;
  Validations validations = new Validations();
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  ScrollController scrollController = new ScrollController();
  bool autoValidate = false;
  var timeout = const Duration(seconds: 30);
  String email;


  void showInSnackBar(String value) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void showProgressDialog() {
    setState(() {
      isInAsyncCall = true;
    });
  }

  void cancelProgressDialog() {
    setState(() {
      isInAsyncCall = false;
    });
  }


  @override
  Widget build(BuildContext context) {
    this.context = context;
    Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      key: scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          "NexiVoy Comercios",
          style: new TextStyle(
            color: white,
            fontSize:
            Theme.of(context).platform == TargetPlatform.iOS ? 17.0 : 20.0,
          ),
        ),
        iconTheme: new IconThemeData(color: Colors.white),
        actions: <Widget>[],
        backgroundColor: primaryColor,
        elevation: Theme.of(context).platform == TargetPlatform.iOS ? 0.0 : 4.0,
      ),
      body: ModalProgressHUD(
        child: SingleChildScrollView(
          controller: scrollController,
          child: new Container(
            height: screenSize.height,
            width: screenSize.width,
            padding: new EdgeInsets.all(8.0),
            child: new Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      'Olvidaste tu contraseña?',
                      style: GoogleFonts.varelaRound(textStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18)),
                    ),
                  ],
                ),
                Container(
                  padding: new EdgeInsets.all(7.0),
                  child: Form(
                    key: formKey,
                    autovalidate: autoValidate,
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(
                          'Nombre',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        new InputField(
                            hintText: 'Escribe tu email',
                            obscureText: false,
                            textInputType: TextInputType.emailAddress,
                            textStyle: textStyle,
                            hintStyle: hintStyle,
                            textCapitalization: TextCapitalization.none,
                            textFieldColor: textFieldColor,
                            bottomMargin: 8.0,
                            validateFunction: validations.validateTextInput,
                            onSaved: (String value) {
                              email = value;
                            }),
                        new RoundedButton(
                          buttonName: "Recuperar contraseña",
                          onTap: handleFormSubmitted,
                          width: screenSize.width,
                          height: screenSize.height / 13,
                          bottomMargin: 10.0,
                          borderWidth: 0.0,
                          buttonColor: primaryColor,
                        ),
                        Text('Revise su email para recuperar su contraseña')
                      ].where((child) => child != null).toList(),
                    ),
                  ),
                ),
              ].where((child) => child != null).toList(),
            ),
          ),
        ),
        inAsyncCall: isInAsyncCall,
        opacity: 0.5,
        progressIndicator: NativeLoadingIndicator(),
      ),
      resizeToAvoidBottomPadding: false,
    );
  }

  void handleFormSubmitted() {
    final FormState form = formKey.currentState;
    if (!form.validate()) {
      autoValidate = true; // Start validating on every change.
      showInSnackBar('Por favor corrija los errores antes de continuar.');
    } else {
      form.save();
      requestPassword();
      //onSelectedCountry();
      //doLogin();
    }
  }

  void requestPassword() async {
    showProgressDialog();
    try {
      var body = {
        'email': email,
        'ts': -1
      };
      Map<String, String> map = {
        'datos': json.encode(body),
      };
      print(map);
      Map<String, String> headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
      };
      final response = await http
          .post('http://' + ApiUrl.urlPrefix + ApiUrl.urlGetVersion,
          headers: headers, body: map)
          .timeout(timeout);
      print(response.body);
      if (response.body != "") {
        handlePasswordResponse(response.body);
      } else {
        handlePasswordResponse("");
      }
    } catch (e) {
      handlePasswordResponse("");
      print('algo salio mal ' + e.toString());
    }
  }

  void handlePasswordResponse(final response) {
    cancelProgressDialog();
    if (response != "") {
      Map<String, dynamic> map = json.decode(response);
      if (map['resultado']['code'] == 0) {
        print(json.encode(map['resultado']));
        try {
          showSuccessMessageDialog();
        } catch (e) {
          print(e.toString());
        }
      } else if (map['resultado']['code'] == -3) {
        showInSnackBar('El usuario introducido no está dado de alta');
      } else if (map['resultado']['code'] == -5) {
        showInSnackBar('Por favor compruebe su conexión');
      }
    } else if (response == "httpExcep") {
      showInSnackBar("Revice la conexion a internet");
    } else {
      showInSnackBar("Algo salio mal");
    }
  }

  void showMessageDialog() {
    MyDialog.showNativePopUpWith2Buttons(
        context,
        'Informacion',
        'Ha ocurrido un problema y no se puede enviar el mensaje',
        'Cancelar',
        'Aceptar');
  }

  void showSuccessMessageDialog() async {
    if (await MyDialog.showNativePopUpWith2Buttons(
        context,
        'Olvido contraseña',
        'Se enviado un correo a su cuenta con la nueva contraseña',
        'Cancelar',
        'Aceptar')) {
      Navigator.pop(context, true);
    }
  }
}