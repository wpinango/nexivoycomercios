
import 'package:nexivoycomercios/screens/account/account_screen.dart';
import 'package:nexivoycomercios/screens/business/business_screen.dart';
import 'package:nexivoycomercios/screens/contact/contact_screen.dart';
import 'package:nexivoycomercios/screens/login/login_screen.dart';
import 'package:nexivoycomercios/screens/main/order_screen.dart';
import 'package:nexivoycomercios/screens/new_order_screen/new_order_screen.dart';
import 'package:nexivoycomercios/screens/pass_recovery/password_recovery_screen.dart';
import 'package:nexivoycomercios/screens/splash/splash_screen.dart';
import 'package:nexivoycomercios/theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Routes {
  var routes = <String, WidgetBuilder> {
    "/Login": (BuildContext context) => new LoginScreen(),
    "/Orders": (BuildContext context) => new OrderScreen(),
    "/NewOrder":(BuildContext context) => new NewOrderScreen(),
    "/Business":(BuildContext context) => new BusinessScreen(),
    "/Account":(BuildContext context) => new AccountScreen(),
    "/Contact":(BuildContext context) => new ContactScreen(),
    "/PasswordRecovery": (BuildContext context) => new PasswordRecoveryScreen(),
  };

  Routes() {
    runApp(new MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "NexiVoy Comercios",
      home: new FirstScreen(),
      routes: routes,
      theme: ThemeData(
        primaryColor: primaryColor,
      ),
    ));
  }

  static replacementScreen(BuildContext context, String routeName) {
    Navigator.pushReplacementNamed(context, routeName);
  }

  static showModalScreen(BuildContext context, String routeName) {
    Navigator.of(context).pushNamed(routeName);
  }
}