import 'package:flutter/material.dart';

Color primaryColor = const Color(0xffb4cb37);
Color white = const Color(0xffffffff);
Color blueNexi = Color(0xff2b94d1);

TextStyle textStyle = const TextStyle(
    color: const Color(0xff000000),
    fontSize: 16.0,
    fontWeight: FontWeight.normal);

TextStyle hintStyle = const TextStyle(
    color: Colors.grey,
    fontSize: 16.0,
    fontWeight: FontWeight.normal);

TextStyle codeStyle = new TextStyle(
  color: const Color(0xfff55020),
  fontSize: 22.0,
  fontWeight: FontWeight.bold,
);

Color whatsappBar = const Color(0xFF41CA50);

Color textFieldColor =  const Color(0xFFEFF0F1);

TextStyle buttonTextStyle = const TextStyle(
    color: Colors.white,
    fontSize: 14.0,
    fontFamily: "Roboto",
    fontWeight: FontWeight.bold);