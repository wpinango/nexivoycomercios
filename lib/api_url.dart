import 'dart:convert';

import 'package:nexivoycomercios/global.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'models/country.dart';
import 'models/login_response.dart';
import 'models/user.dart';
import 'package:http/http.dart' as http;

class ApiUrl {
  static final String urlPrefix = Global.isProduction
      ? 'app.nexivoy.com/administracion/ws/city'
      : 'cityboxapp.com/nexivoy/backend/ws/city';
  static final String urlLogin = '/?/ws_getAuth.php';
  static final String urlGetVersion = '/ws_getVersion.php';
  static final String urlSetTicket = '/?/ws_setTicket.php';
  static final String urlGetOrders = '/?/ws_getOrders.php';
  static final String urlLogout = "/?/ws_setLogout.php";
  static final String urlGetProfile = '/?/ws_getProfile.php';
  static final String urlUpdateProfile = '/?/ws_setProfile.php';
  static final String urlSetOrder = '/?/ws_setOrder.php';
  static final String trackOrder = '/?/ws_getInfoFreelance.php';
  static final String urlSetOrderDelivered = '/?/ws_setStateOrder.php';
  static final String ulrSetPush = '/?/ws_setPushService.php';

  static void setPushService() async {
    Country selectedCountry;
    LoginResponse loginResponse;
    final prefs = await SharedPreferences.getInstance();
    if (prefs.get('selectedCountry') != null) {
      selectedCountry =
          Country.fromJson(json.decode(prefs.get('selectedCountry')));
    }
    if (prefs.getString('loginData') != null) {
      loginResponse =
          LoginResponse.fromJson(json.decode(prefs.get('loginData')));
    }
    String fbToken = prefs.getString('fbToken') ?? '-1';
    if (selectedCountry != null && loginResponse != null) {
      if (loginResponse.token != '-1' && fbToken != '-1') {
        var body = {
          'push': fbToken,
          'token': loginResponse.token,
        };
        Map<String, String> map = {
          'datos': json.encode(body),
        };
        print(map);
        Map<String, String> headers = {
          'Content-Type': 'application/x-www-form-urlencoded',
        };
        String apiVersion = prefs.get('apiVersion');
        try {
          final response = await http
              .post(
              selectedCountry.protocol +
                  '://' +
                  selectedCountry.url +
                  ApiUrl.ulrSetPush.replaceAll('?', apiVersion),
              headers: headers,
              body: map)
              .timeout(Global.timeout);
          print(response.body);
          Map<String, dynamic> mp = json.decode(response.body);
          print(mp);
        } catch (e) {
          e.toString();
        }
      }
    }
  }
}